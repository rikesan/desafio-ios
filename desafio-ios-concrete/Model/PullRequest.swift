//
//  PullRequest.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

struct PullRequest: Codable {
    
    var title: String
    var body: String?
    var user: User
    var url: URL
    var state: String
    
    enum CodingKeys: String, CodingKey {
        case title
        case body
        case user
        case url = "html_url"
        case state
    }
    
}
