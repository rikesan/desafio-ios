//
//  Repository.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 31/10/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

struct Repository: Codable {
    
    var id: Int
    var name: String
    var description: String?
    var stars: Int
    var forks: Int
    var owner: User
    var pullRequestsURL: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case stars = "stargazers_count"
        case forks = "forks_count"
        case owner
        case pullRequestsURL = "pulls_url"
    }
}

