//
//  RepositoryPage.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 04/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

struct RepositoryPage: Codable {
    
    var count: Int
    var items: [Repository]
    
    enum CodingKeys: String, CodingKey {
        case count = "total_count"
        case items
    }
    
}
