//
//  RepositoryOwner.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 04/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

struct User: Codable {
    
    var login: String
    var id: Int
    var avatarURL: URL
    
    enum CodingKeys: String, CodingKey {
        case login
        case id
        case avatarURL = "avatar_url"
    }
}
