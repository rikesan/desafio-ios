//
//  API.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

typealias RepositoriesCallback = (Result<RepositoryPage>) -> Void
typealias PullRequestsCallback = (Result<[PullRequest]>) -> Void

class NetworkAPI {
    
    static let repoEndpoint = "https://api.github.com/search/repositories?q=language:Java&sort=stars"
    static let pageRedirect = "&page"
    static let prNumReplace = "{/number}"
    static let stateRedirect = "?state=all"
    
    static func fetchRepositories(page: Int, completion: @escaping RepositoriesCallback) {
        let endpointPaged = "\(repoEndpoint)\(pageRedirect)\(page)"
        
        Network.fetch(link: endpointPaged) { result in
            switch result {
            case .success(let data):
                do {
                    let parsedData = try NetworkHelper.decode(data: data, into: RepositoryPage.self)
                    completion(.success(parsedData))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
    static func fetchPRs(endpoint: String, completion: @escaping ((Result<[PullRequest]>) -> Void)) {
        let trimmedEndpoint = endpoint.replacingOccurrences(of: prNumReplace, with: "")
        let endpointWithStateFilter = "\(trimmedEndpoint)\(stateRedirect)"
        Network.fetch(link: endpointWithStateFilter) { result in
            switch result {
            case .success(let data):
                do {
                    let parsedData = try NetworkHelper.decode(data: data, into: [PullRequest].self)
                    completion(.success(parsedData))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }

}
