//
//  RepositoriesNetwork.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 30/10/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit.UIImage

typealias NetworkCallback = (Result<Data>) -> Void

class Network {
    
    static func fetch(url: URL, completion: @escaping NetworkCallback) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            }
            
            guard let unwrappedData = data else {
                completion(.failure(NetworkError.generic))
                return
            }
            
            DispatchQueue.main.async {
                completion(.success(unwrappedData))
            }
        })
        
        task.resume()
    }
    
    static func fetch(link: String, completion: @escaping NetworkCallback) {
        guard let url = URL(string: link) else {
            completion(.failure(NetworkError.urlNotValid))
            return
        }
        
        Network.fetch(url: url, completion: completion)
    }
}

enum NetworkError: Error {
    case generic
    case urlNotValid
}
