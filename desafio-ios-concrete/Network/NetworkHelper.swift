//
//  NetworkHelper.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

class NetworkHelper {
    
    class func decode<T>(data: Data, into type: T.Type) throws -> T where T: Codable {
        let decoder = JSONDecoder()
        let responseObj = try decoder.decode(T.self, from: data)
        return responseObj
    }
    
}
