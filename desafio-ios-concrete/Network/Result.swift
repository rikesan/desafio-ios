//
//  Result.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 04/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}
