//
//  PRsViewController.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 04/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

class PRsViewController: UIViewController {
    
    let repository: Repository
    var pullRequests: [PullRequest]
    private var indicator: UIActivityIndicatorView
    private let tableView: UITableView
    private let cellID = "PRCellID"
    private let header: UIView
    private let headerLabel: UILabel
    private var pullsOpened: Int = 0
    private var pullsClosed: Int = 0
    
    init(withRepository repository: Repository) {
        self.repository = repository
        indicator = UIActivityIndicatorView()
        tableView = UITableView()
        pullRequests = [PullRequest]()
        header = UIView()
        headerLabel = UILabel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        self.title = repository.name
        view.backgroundColor = .white
        fetchPullRequests()
        setupActivityIndicator()
        setupTableView()
    }
    
    private func setupActivityIndicator() {
        self.view.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.activityIndicatorViewStyle = .gray
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        indicator.startAnimating()
        indicator.hidesWhenStopped = true
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(PRCell.self, forCellReuseIdentifier: cellID)
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.isHidden = true
        setupTableViewConstraints()
    }
    
    private func setupTableViewConstraints() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let margins = view.layoutMarginsGuide
        tableView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    private func fetchPullRequests() {
        NetworkAPI.fetchPRs(endpoint: repository.pullRequestsURL) { (result) in
            switch result {
            case .success(let prs):
                DispatchQueue.main.async {
                    self.pullRequests = prs
                    self.tableView.reloadData()
                    self.indicator.stopAnimating()
                    self.tableView.isHidden = false
                    self.hasZeroPRs()
                    self.pullsOpened = self.pullRequests.filter{ $0.state == "open"}.count
                    self.pullsClosed = self.pullRequests.filter{ $0.state == "closed"}.count
                    self.headerLabel.text = "\(self.pullsOpened) open / \(self.pullsClosed) closed"
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.errorAlert(message: "Failed to fetch Pull Requests")
            }
        }
    }
    
    private func hasZeroPRs() {
        if self.pullRequests.count == 0 {
            self.errorAlert(message: "Repository does not have any Pull Requests")
        }
    }
}

extension PRsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = pullRequests[indexPath.row].url
        UIApplication.shared.openURL(url)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.textAlignment = .center
        label.text = "\(self.pullsOpened) open / \(self.pullsClosed) closed"
        label.backgroundColor = .white
        return label
    }
    
}

extension PRsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return pullRequests.count > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        
        guard let cell = genericCell as? PRCell else {
            return genericCell
        }
        
        cell.titleLabel.text = pullRequests[indexPath.row].title
        cell.bodyLabel.text = pullRequests[indexPath.row].body
        cell.userImage.downloadImage(url: pullRequests[indexPath.row].user.avatarURL)
        cell.usernameLabel.text = pullRequests[indexPath.row].user.login
        
        return cell
    }
    
}
