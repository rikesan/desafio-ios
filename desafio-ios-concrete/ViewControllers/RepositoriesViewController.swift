//
//  ViewController.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 30/10/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import UIKit

class RepositoriesViewController: UIViewController {

    var repositories: [Repository]
    private let tableView: UITableView
    private let cellIdentifier = "cellID"
    private var currentPage = 1
    private var shouldShowLoadingCell = false
    private var indicator: UIActivityIndicatorView
    private var totalNumberOfRepositories: Int?
    
    init() {
        tableView = UITableView()
        indicator = UIActivityIndicatorView()
        repositories = [Repository]()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRepositories()
        view.backgroundColor = .white
        setupNavbar()
        setupView()
        setupActivityIndicator()
    }
    
    private func setupView() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(RepositoryCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.isHidden = true
        setupTableViewConstraints()
    }
    
    private func setupActivityIndicator() {
        self.view.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.activityIndicatorViewStyle = .gray
        indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        indicator.startAnimating()
        indicator.hidesWhenStopped = true
    }
    
    private func setupTableViewConstraints() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let margins = view.layoutMarginsGuide
        tableView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    private func setupNavbar() {
        self.title = "Github Java"
        navigationController?.navigationBar.barStyle = .black
        let navBarColor = UIColor(red: 0.2039, green: 0.2039, blue: 0.2196, alpha: 1)
        navigationController?.navigationBar.barTintColor = navBarColor
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
        }
    }
    
    private func fetchRepositories() {
        NetworkAPI.fetchRepositories(page: currentPage) { (result) in
            switch result {
            case .success(let page):
                DispatchQueue.main.async {
                    self.repositories.append(contentsOf: page.items)
                    
                    if self.totalNumberOfRepositories == nil {
                        self.totalNumberOfRepositories = page.count
                    }
                    
                    self.shouldShowLoadingCell = self.hasMorePagesToFetch()
                    self.tableView.reloadData()
                    self.indicator.stopAnimating()
                    self.tableView.isHidden = false
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.errorAlert(message: "Failed to fetch repositories")
            }
        }
    }
    
    private func hasMorePagesToFetch() -> Bool {
        if let numRepos = totalNumberOfRepositories {
            return repositories.count < numRepos
        }
        
        return false
    }
    
    private func fetchNextPage() {
        currentPage += 1
        fetchRepositories()
    }

}

extension RepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath: indexPath) else {
            return
        }
        
        fetchNextPage()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repo = repositories[indexPath.row]
        let prVC = PRsViewController(withRepository: repo)
        navigationController?.pushViewController(prVC, animated: true)
    }
}

extension RepositoriesViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = repositories.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let genericCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        guard let cell = genericCell as? RepositoryCell else {
            return genericCell
        }
        
        if isLoadingIndexPath(indexPath: indexPath) {
            return LoadingCell(style: .default, reuseIdentifier: "loading")
        } else {
            cell.nameLabel.text = repositories[indexPath.row].name
            cell.descriptionLabel.text = repositories[indexPath.row].description
            cell.userImage.downloadImage(url: repositories[indexPath.row].owner.avatarURL)
            cell.forkLabel.text = "\(repositories[indexPath.row].forks)"
            cell.starsLabel.text = "\(repositories[indexPath.row].stars)"
            cell.usernameLabel.text = repositories[indexPath.row].owner.login
            cell.selectionStyle = .none
        }
    
        return cell
    }

    func isLoadingIndexPath(indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else {
            return false
        }
        
        return indexPath.row == repositories.count
    }
}
