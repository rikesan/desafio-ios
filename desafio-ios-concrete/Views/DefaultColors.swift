//
//  DefaultColors.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

class DefaultColors {
    static let blue = UIColor(red: 0.2118, green: 0.4275, blue: 0.6392, alpha: 1)
    static let beige = UIColor(red: 0.8353, green: 0.5843, blue: 0.2039, alpha: 1)
}

