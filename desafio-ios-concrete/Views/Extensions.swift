//
//  Extensions.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func downloadImage(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        Network.fetch(url: url) { (result) in
            switch result {
            case .success(let data):
                if let image = UIImage(data: data) {
                    self.image = image
                    self.contentMode = mode
                }
            case .failure:
                break
            }
        }
    }
    
}

extension UIViewController {
    
    func errorAlert(message: String?) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        
    }
}
