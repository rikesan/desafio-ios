//
//  Icon.swift
//  forms-layout
//
//  Created by Henrique Santiago on 05/06/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

class Icon {
    
    static let ioniconsFontName = "Ionicons"
    
    class func getIcon(_ icon: Icons, fontSize: CGFloat = UIFont.systemFontSize) -> NSAttributedString? {
        guard let font = UIFont(name: ioniconsFontName, size: fontSize) else {
            print("Could not find icon font name")
            return nil
        }
    
        guard let code = getIconCode(icon) else {
            print("Could not find icon code")
            return nil
        }
        
        let attributedText = NSAttributedString(string: code, attributes: [NSAttributedStringKey.font: font])
        return attributedText
    }
    
    private class func getIconCode(_ icon: Icons) -> String? {
        let iconName = icon.rawValue
        let iconCode = ioniconsCodes[iconName]
        return iconCode
    }
    
}

enum Icons: String {
    case ion_alert = "ion-alert"
    case ion_alert_circled = "ion-alert-circled"
    case ion_android_add = "ion-android-add"
    case ion_android_add_circle = "ion-android-add-circle"
    case ion_android_alarm_clock = "ion-android-alarm-clock"
    case ion_android_alert = "ion-android-alert"
    case ion_android_apps = "ion-android-apps"
    case ion_android_archive = "ion-android-archive"
    case ion_android_arrow_back = "ion-android-arrow-back"
    case ion_android_arrow_down = "ion-android-arrow-down"
    case ion_android_arrow_dropdown = "ion-android-arrow-dropdown"
    case ion_android_arrow_dropdown_circle = "ion-android-arrow-dropdown-circle"
    case ion_android_arrow_dropleft = "ion-android-arrow-dropleft"
    case ion_android_arrow_dropleft_circle = "ion-android-arrow-dropleft-circle"
    case ion_android_arrow_dropright = "ion-android-arrow-dropright"
    case ion_android_arrow_dropright_circle = "ion-android-arrow-dropright-circle"
    case ion_android_arrow_dropup = "ion-android-arrow-dropup"
    case ion_android_arrow_dropup_circle = "ion-android-arrow-dropup-circle"
    case ion_android_arrow_forward = "ion-android-arrow-forward"
    case ion_android_arrow_up = "ion-android-arrow-up"
    case ion_android_attach = "ion-android-attach"
    case ion_android_bar = "ion-android-bar"
    case ion_android_bicycle = "ion-android-bicycle"
    case ion_android_boat = "ion-android-boat"
    case ion_android_bookmark = "ion-android-bookmark"
    case ion_android_bulb = "ion-android-bulb"
    case ion_android_bus = "ion-android-bus"
    case ion_android_calendar = "ion-android-calendar"
    case ion_android_call = "ion-android-call"
    case ion_android_camera = "ion-android-camera"
    case ion_android_cancel = "ion-android-cancel"
    case ion_android_car = "ion-android-car"
    case ion_android_cart = "ion-android-cart"
    case ion_android_chat = "ion-android-chat"
    case ion_android_checkbox = "ion-android-checkbox"
    case ion_android_checkbox_blank = "ion-android-checkbox-blank"
    case ion_android_checkbox_outline = "ion-android-checkbox-outline"
    case ion_android_checkbox_outline_blank = "ion-android-checkbox-outline-blank"
    case ion_android_checkmark_circle = "ion-android-checkmark-circle"
    case ion_android_clipboard = "ion-android-clipboard"
    case ion_android_close = "ion-android-close"
    case ion_android_cloud = "ion-android-cloud"
    case ion_android_cloud_circle = "ion-android-cloud-circle"
    case ion_android_cloud_done = "ion-android-cloud-done"
    case ion_android_cloud_outline = "ion-android-cloud-outline"
    case ion_android_color_palette = "ion-android-color-palette"
    case ion_android_compass = "ion-android-compass"
    case ion_android_contact = "ion-android-contact"
    case ion_android_contacts = "ion-android-contacts"
    case ion_android_contract = "ion-android-contract"
    case ion_android_create = "ion-android-create"
    case ion_android_delete = "ion-android-delete"
    case ion_android_desktop = "ion-android-desktop"
    case ion_android_document = "ion-android-document"
    case ion_android_done = "ion-android-done"
    case ion_android_done_all = "ion-android-done-all"
    case ion_android_download = "ion-android-download"
    case ion_android_drafts = "ion-android-drafts"
    case ion_android_exit = "ion-android-exit"
    case ion_android_expand = "ion-android-expand"
    case ion_android_favorite = "ion-android-favorite"
    case ion_android_favorite_outline = "ion-android-favorite-outline"
    case ion_android_film = "ion-android-film"
    case ion_android_folder = "ion-android-folder"
    case ion_android_folder_open = "ion-android-folder-open"
    case ion_android_funnel = "ion-android-funnel"
    case ion_android_globe = "ion-android-globe"
    case ion_android_hand = "ion-android-hand"
    case ion_android_hangout = "ion-android-hangout"
    case ion_android_happy = "ion-android-happy"
    case ion_android_home = "ion-android-home"
    case ion_android_image = "ion-android-image"
    case ion_android_laptop = "ion-android-laptop"
    case ion_android_list = "ion-android-list"
    case ion_android_locate = "ion-android-locate"
    case ion_android_lock = "ion-android-lock"
    case ion_android_mail = "ion-android-mail"
    case ion_android_map = "ion-android-map"
    case ion_android_menu = "ion-android-menu"
    case ion_android_microphone = "ion-android-microphone"
    case ion_android_microphone_off = "ion-android-microphone-off"
    case ion_android_more_horizontal = "ion-android-more-horizontal"
    case ion_android_more_vertical = "ion-android-more-vertical"
    case ion_android_navigate = "ion-android-navigate"
    case ion_android_notifications = "ion-android-notifications"
    case ion_android_notifications_none = "ion-android-notifications-none"
    case ion_android_notifications_off = "ion-android-notifications-off"
    case ion_android_open = "ion-android-open"
    case ion_android_options = "ion-android-options"
    case ion_android_people = "ion-android-people"
    case ion_android_person = "ion-android-person"
    case ion_android_person_add = "ion-android-person-add"
    case ion_android_phone_landscape = "ion-android-phone-landscape"
    case ion_android_phone_portrait = "ion-android-phone-portrait"
    case ion_android_pin = "ion-android-pin"
    case ion_android_plane = "ion-android-plane"
    case ion_android_playstore = "ion-android-playstore"
    case ion_android_print = "ion-android-print"
    case ion_android_radio_button_off = "ion-android-radio-button-off"
    case ion_android_radio_button_on = "ion-android-radio-button-on"
    case ion_android_refresh = "ion-android-refresh"
    case ion_android_remove = "ion-android-remove"
    case ion_android_remove_circle = "ion-android-remove-circle"
    case ion_android_restaurant = "ion-android-restaurant"
    case ion_android_sad = "ion-android-sad"
    case ion_android_search = "ion-android-search"
    case ion_android_send = "ion-android-send"
    case ion_android_settings = "ion-android-settings"
    case ion_android_share = "ion-android-share"
    case ion_android_share_alt = "ion-android-share-alt"
    case ion_android_star = "ion-android-star"
    case ion_android_star_half = "ion-android-star-half"
    case ion_android_star_outline = "ion-android-star-outline"
    case ion_android_stopwatch = "ion-android-stopwatch"
    case ion_android_subway = "ion-android-subway"
    case ion_android_sunny = "ion-android-sunny"
    case ion_android_sync = "ion-android-sync"
    case ion_android_textsms = "ion-android-textsms"
    case ion_android_time = "ion-android-time"
    case ion_android_train = "ion-android-train"
    case ion_android_unlock = "ion-android-unlock"
    case ion_android_upload = "ion-android-upload"
    case ion_android_volume_down = "ion-android-volume-down"
    case ion_android_volume_mute = "ion-android-volume-mute"
    case ion_android_volume_off = "ion-android-volume-off"
    case ion_android_volume_up = "ion-android-volume-up"
    case ion_android_walk = "ion-android-walk"
    case ion_android_warning = "ion-android-warning"
    case ion_android_watch = "ion-android-watch"
    case ion_android_wifi = "ion-android-wifi"
    case ion_aperture = "ion-aperture"
    case ion_archive = "ion-archive"
    case ion_arrow_down_a = "ion-arrow-down-a"
    case ion_arrow_down_b = "ion-arrow-down-b"
    case ion_arrow_down_c = "ion-arrow-down-c"
    case ion_arrow_expand = "ion-arrow-expand"
    case ion_arrow_graph_down_left = "ion-arrow-graph-down-left"
    case ion_arrow_graph_down_right = "ion-arrow-graph-down-right"
    case ion_arrow_graph_up_left = "ion-arrow-graph-up-left"
    case ion_arrow_graph_up_right = "ion-arrow-graph-up-right"
    case ion_arrow_left_a = "ion-arrow-left-a"
    case ion_arrow_left_b = "ion-arrow-left-b"
    case ion_arrow_left_c = "ion-arrow-left-c"
    case ion_arrow_move = "ion-arrow-move"
    case ion_arrow_resize = "ion-arrow-resize"
    case ion_arrow_return_left = "ion-arrow-return-left"
    case ion_arrow_return_right = "ion-arrow-return-right"
    case ion_arrow_right_a = "ion-arrow-right-a"
    case ion_arrow_right_b = "ion-arrow-right-b"
    case ion_arrow_right_c = "ion-arrow-right-c"
    case ion_arrow_shrink = "ion-arrow-shrink"
    case ion_arrow_swap = "ion-arrow-swap"
    case ion_arrow_up_a = "ion-arrow-up-a"
    case ion_arrow_up_b = "ion-arrow-up-b"
    case ion_arrow_up_c = "ion-arrow-up-c"
    case ion_asterisk = "ion-asterisk"
    case ion_at = "ion-at"
    case ion_backspace = "ion-backspace"
    case ion_backspace_outline = "ion-backspace-outline"
    case ion_bag = "ion-bag"
    case ion_battery_charging = "ion-battery-charging"
    case ion_battery_empty = "ion-battery-empty"
    case ion_battery_full = "ion-battery-full"
    case ion_battery_half = "ion-battery-half"
    case ion_battery_low = "ion-battery-low"
    case ion_beaker = "ion-beaker"
    case ion_beer = "ion-beer"
    case ion_bluetooth = "ion-bluetooth"
    case ion_bonfire = "ion-bonfire"
    case ion_bookmark = "ion-bookmark"
    case ion_bowtie = "ion-bowtie"
    case ion_brief = "ion-brief"
    case ion_bug = "ion-bug"
    case ion_calculator = "ion-calculator"
    case ion_calendar = "ion-calendar"
    case ion_camera = "ion-camera"
    case ion_card = "ion-card"
    case ion_cash = "ion-cash"
    case ion_chatbox = "ion-chatbox"
    case ion_chatbox_working = "ion-chatbox-working"
    case ion_chatboxes = "ion-chatboxes"
    case ion_chatbubble = "ion-chatbubble"
    case ion_chatbubble_working = "ion-chatbubble-working"
    case ion_chatbubbles = "ion-chatbubbles"
    case ion_checkmark = "ion-checkmark"
    case ion_checkmark_circled = "ion-checkmark-circled"
    case ion_checkmark_round = "ion-checkmark-round"
    case ion_chevron_down = "ion-chevron-down"
    case ion_chevron_left = "ion-chevron-left"
    case ion_chevron_right = "ion-chevron-right"
    case ion_chevron_up = "ion-chevron-up"
    case ion_clipboard = "ion-clipboard"
    case ion_clock = "ion-clock"
    case ion_close = "ion-close"
    case ion_close_circled = "ion-close-circled"
    case ion_close_round = "ion-close-round"
    case ion_closed_captioning = "ion-closed-captioning"
    case ion_cloud = "ion-cloud"
    case ion_code = "ion-code"
    case ion_code_download = "ion-code-download"
    case ion_code_working = "ion-code-working"
    case ion_coffee = "ion-coffee"
    case ion_compass = "ion-compass"
    case ion_compose = "ion-compose"
    case ion_connection_bars = "ion-connection-bars"
    case ion_contrast = "ion-contrast"
    case ion_crop = "ion-crop"
    case ion_cube = "ion-cube"
    case ion_disc = "ion-disc"
    case ion_document = "ion-document"
    case ion_document_text = "ion-document-text"
    case ion_drag = "ion-drag"
    case ion_earth = "ion-earth"
    case ion_easel = "ion-easel"
    case ion_edit = "ion-edit"
    case ion_egg = "ion-egg"
    case ion_eject = "ion-eject"
    case ion_email = "ion-email"
    case ion_email_unread = "ion-email-unread"
    case ion_erlenmeyer_flask = "ion-erlenmeyer-flask"
    case ion_erlenmeyer_flask_bubbles = "ion-erlenmeyer-flask-bubbles"
    case ion_eye = "ion-eye"
    case ion_eye_disabled = "ion-eye-disabled"
    case ion_female = "ion-female"
    case ion_filing = "ion-filing"
    case ion_film_marker = "ion-film-marker"
    case ion_fireball = "ion-fireball"
    case ion_flag = "ion-flag"
    case ion_flame = "ion-flame"
    case ion_flash = "ion-flash"
    case ion_flash_off = "ion-flash-off"
    case ion_folder = "ion-folder"
    case ion_fork = "ion-fork"
    case ion_fork_repo = "ion-fork-repo"
    case ion_forward = "ion-forward"
    case ion_funnel = "ion-funnel"
    case ion_gear_a = "ion-gear-a"
    case ion_gear_b = "ion-gear-b"
    case ion_grid = "ion-grid"
    case ion_hammer = "ion-hammer"
    case ion_happy = "ion-happy"
    case ion_happy_outline = "ion-happy-outline"
    case ion_headphone = "ion-headphone"
    case ion_heart = "ion-heart"
    case ion_heart_broken = "ion-heart-broken"
    case ion_help = "ion-help"
    case ion_help_buoy = "ion-help-buoy"
    case ion_help_circled = "ion-help-circled"
    case ion_home = "ion-home"
    case ion_icecream = "ion-icecream"
    case ion_image = "ion-image"
    case ion_images = "ion-images"
    case ion_information = "ion-information"
    case ion_information_circled = "ion-information-circled"
    case ion_ionic = "ion-ionic"
    case ion_ios_alarm = "ion-ios-alarm"
    case ion_ios_alarm_outline = "ion-ios-alarm-outline"
    case ion_ios_albums = "ion-ios-albums"
    case ion_ios_albums_outline = "ion-ios-albums-outline"
    case ion_ios_americanfootball = "ion-ios-americanfootball"
    case ion_ios_americanfootball_outline = "ion-ios-americanfootball-outline"
    case ion_ios_analytics = "ion-ios-analytics"
    case ion_ios_analytics_outline = "ion-ios-analytics-outline"
    case ion_ios_arrow_back = "ion-ios-arrow-back"
    case ion_ios_arrow_down = "ion-ios-arrow-down"
    case ion_ios_arrow_forward = "ion-ios-arrow-forward"
    case ion_ios_arrow_left = "ion-ios-arrow-left"
    case ion_ios_arrow_right = "ion-ios-arrow-right"
    case ion_ios_arrow_thin_down = "ion-ios-arrow-thin-down"
    case ion_ios_arrow_thin_left = "ion-ios-arrow-thin-left"
    case ion_ios_arrow_thin_right = "ion-ios-arrow-thin-right"
    case ion_ios_arrow_thin_up = "ion-ios-arrow-thin-up"
    case ion_ios_arrow_up = "ion-ios-arrow-up"
    case ion_ios_at = "ion-ios-at"
    case ion_ios_at_outline = "ion-ios-at-outline"
    case ion_ios_barcode = "ion-ios-barcode"
    case ion_ios_barcode_outline = "ion-ios-barcode-outline"
    case ion_ios_baseball = "ion-ios-baseball"
    case ion_ios_baseball_outline = "ion-ios-baseball-outline"
    case ion_ios_basketball = "ion-ios-basketball"
    case ion_ios_basketball_outline = "ion-ios-basketball-outline"
    case ion_ios_bell = "ion-ios-bell"
    case ion_ios_bell_outline = "ion-ios-bell-outline"
    case ion_ios_body = "ion-ios-body"
    case ion_ios_body_outline = "ion-ios-body-outline"
    case ion_ios_bolt = "ion-ios-bolt"
    case ion_ios_bolt_outline = "ion-ios-bolt-outline"
    case ion_ios_book = "ion-ios-book"
    case ion_ios_book_outline = "ion-ios-book-outline"
    case ion_ios_bookmarks = "ion-ios-bookmarks"
    case ion_ios_bookmarks_outline = "ion-ios-bookmarks-outline"
    case ion_ios_box = "ion-ios-box"
    case ion_ios_box_outline = "ion-ios-box-outline"
    case ion_ios_brief = "ion-ios-brief"
    case ion_ios_briefcase_outline = "ion-ios-briefcase-outline"
    case ion_ios_browsers = "ion-ios-browsers"
    case ion_ios_browsers_outline = "ion-ios-browsers-outline"
    case ion_ios_calculator = "ion-ios-calculator"
    case ion_ios_calculator_outline = "ion-ios-calculator-outline"
    case ion_ios_calendar = "ion-ios-calendar"
    case ion_ios_calendar_outline = "ion-ios-calendar-outline"
    case ion_ios_camera = "ion-ios-camera"
    case ion_ios_camera_outline = "ion-ios-camera-outline"
    case ion_ios_cart = "ion-ios-cart"
    case ion_ios_cart_outline = "ion-ios-cart-outline"
    case ion_ios_chatboxes = "ion-ios-chatboxes"
    case ion_ios_chatboxes_outline = "ion-ios-chatboxes-outline"
    case ion_ios_chatbubble = "ion-ios-chatbubble"
    case ion_ios_chatbubble_outline = "ion-ios-chatbubble-outline"
    case ion_ios_checkmark = "ion-ios-checkmark"
    case ion_ios_checkmark_empty = "ion-ios-checkmark-empty"
    case ion_ios_checkmark_outline = "ion-ios-checkmark-outline"
    case ion_ios_circle_filled = "ion-ios-circle-filled"
    case ion_ios_circle_outline = "ion-ios-circle-outline"
    case ion_ios_clock = "ion-ios-clock"
    case ion_ios_clock_outline = "ion-ios-clock-outline"
    case ion_ios_close = "ion-ios-close"
    case ion_ios_close_empty = "ion-ios-close-empty"
    case ion_ios_close_outline = "ion-ios-close-outline"
    case ion_ios_cloud = "ion-ios-cloud"
    case ion_ios_cloud_download = "ion-ios-cloud-download"
    case ion_ios_cloud_download_outline = "ion-ios-cloud-download-outline"
    case ion_ios_cloud_outline = "ion-ios-cloud-outline"
    case ion_ios_cloud_upload = "ion-ios-cloud-upload"
    case ion_ios_cloud_upload_outline = "ion-ios-cloud-upload-outline"
    case ion_ios_cloudy = "ion-ios-cloudy"
    case ion_ios_cloudy_night = "ion-ios-cloudy-night"
    case ion_ios_cloudy_night_outline = "ion-ios-cloudy-night-outline"
    case ion_ios_cloudy_outline = "ion-ios-cloudy-outline"
    case ion_ios_cog = "ion-ios-cog"
    case ion_ios_cog_outline = "ion-ios-cog-outline"
    case ion_ios_color_filter = "ion-ios-color-filter"
    case ion_ios_color_filter_outline = "ion-ios-color-filter-outline"
    case ion_ios_color_wand = "ion-ios-color-wand"
    case ion_ios_color_wand_outline = "ion-ios-color-wand-outline"
    case ion_ios_compose = "ion-ios-compose"
    case ion_ios_compose_outline = "ion-ios-compose-outline"
    case ion_ios_contact = "ion-ios-contact"
    case ion_ios_contact_outline = "ion-ios-contact-outline"
    case ion_ios_copy = "ion-ios-copy"
    case ion_ios_copy_outline = "ion-ios-copy-outline"
    case ion_ios_crop = "ion-ios-crop"
    case ion_ios_crop_strong = "ion-ios-crop-strong"
    case ion_ios_download = "ion-ios-download"
    case ion_ios_download_outline = "ion-ios-download-outline"
    case ion_ios_drag = "ion-ios-drag"
    case ion_ios_email = "ion-ios-email"
    case ion_ios_email_outline = "ion-ios-email-outline"
    case ion_ios_eye = "ion-ios-eye"
    case ion_ios_eye_outline = "ion-ios-eye-outline"
    case ion_ios_fastforward = "ion-ios-fastforward"
    case ion_ios_fastforward_outline = "ion-ios-fastforward-outline"
    case ion_ios_filing = "ion-ios-filing"
    case ion_ios_filing_outline = "ion-ios-filing-outline"
    case ion_ios_film = "ion-ios-film"
    case ion_ios_film_outline = "ion-ios-film-outline"
    case ion_ios_flag = "ion-ios-flag"
    case ion_ios_flag_outline = "ion-ios-flag-outline"
    case ion_ios_flame = "ion-ios-flame"
    case ion_ios_flame_outline = "ion-ios-flame-outline"
    case ion_ios_flask = "ion-ios-flask"
    case ion_ios_flask_outline = "ion-ios-flask-outline"
    case ion_ios_flower = "ion-ios-flower"
    case ion_ios_flower_outline = "ion-ios-flower-outline"
    case ion_ios_folder = "ion-ios-folder"
    case ion_ios_folder_outline = "ion-ios-folder-outline"
    case ion_ios_football = "ion-ios-football"
    case ion_ios_football_outline = "ion-ios-football-outline"
    case ion_ios_game_controller_a = "ion-ios-game-controller-a"
    case ion_ios_game_controller_a_outline = "ion-ios-game-controller-a-outline"
    case ion_ios_game_controller_b = "ion-ios-game-controller-b"
    case ion_ios_game_controller_b_outline = "ion-ios-game-controller-b-outline"
    case ion_ios_gear = "ion-ios-gear"
    case ion_ios_gear_outline = "ion-ios-gear-outline"
    case ion_ios_glasses = "ion-ios-glasses"
    case ion_ios_glasses_outline = "ion-ios-glasses-outline"
    case ion_ios_grid_view = "ion-ios-grid-view"
    case ion_ios_grid_view_outline = "ion-ios-grid-view-outline"
    case ion_ios_heart = "ion-ios-heart"
    case ion_ios_heart_outline = "ion-ios-heart-outline"
    case ion_ios_help = "ion-ios-help"
    case ion_ios_help_empty = "ion-ios-help-empty"
    case ion_ios_help_outline = "ion-ios-help-outline"
    case ion_ios_home = "ion-ios-home"
    case ion_ios_home_outline = "ion-ios-home-outline"
    case ion_ios_infinite = "ion-ios-infinite"
    case ion_ios_infinite_outline = "ion-ios-infinite-outline"
    case ion_ios_information = "ion-ios-information"
    case ion_ios_information_empty = "ion-ios-information-empty"
    case ion_ios_information_outline = "ion-ios-information-outline"
    case ion_ios_ionic_outline = "ion-ios-ionic-outline"
    case ion_ios_keypad = "ion-ios-keypad"
    case ion_ios_keypad_outline = "ion-ios-keypad-outline"
    case ion_ios_lightbulb = "ion-ios-lightbulb"
    case ion_ios_lightbulb_outline = "ion-ios-lightbulb-outline"
    case ion_ios_list = "ion-ios-list"
    case ion_ios_list_outline = "ion-ios-list-outline"
    case ion_ios_location = "ion-ios-location"
    case ion_ios_location_outline = "ion-ios-location-outline"
    case ion_ios_locked = "ion-ios-locked"
    case ion_ios_locked_outline = "ion-ios-locked-outline"
    case ion_ios_loop = "ion-ios-loop"
    case ion_ios_loop_strong = "ion-ios-loop-strong"
    case ion_ios_medical = "ion-ios-medical"
    case ion_ios_medical_outline = "ion-ios-medical-outline"
    case ion_ios_medkit = "ion-ios-medkit"
    case ion_ios_medkit_outline = "ion-ios-medkit-outline"
    case ion_ios_mic = "ion-ios-mic"
    case ion_ios_mic_off = "ion-ios-mic-off"
    case ion_ios_mic_outline = "ion-ios-mic-outline"
    case ion_ios_minus = "ion-ios-minus"
    case ion_ios_minus_empty = "ion-ios-minus-empty"
    case ion_ios_minus_outline = "ion-ios-minus-outline"
    case ion_ios_monitor = "ion-ios-monitor"
    case ion_ios_monitor_outline = "ion-ios-monitor-outline"
    case ion_ios_moon = "ion-ios-moon"
    case ion_ios_moon_outline = "ion-ios-moon-outline"
    case ion_ios_more = "ion-ios-more"
    case ion_ios_more_outline = "ion-ios-more-outline"
    case ion_ios_musical_note = "ion-ios-musical-note"
    case ion_ios_musical_notes = "ion-ios-musical-notes"
    case ion_ios_navigate = "ion-ios-navigate"
    case ion_ios_navigate_outline = "ion-ios-navigate-outline"
    case ion_ios_nutrition = "ion-ios-nutrition"
    case ion_ios_nutrition_outline = "ion-ios-nutrition-outline"
    case ion_ios_paper = "ion-ios-paper"
    case ion_ios_paper_outline = "ion-ios-paper-outline"
    case ion_ios_paperplane = "ion-ios-paperplane"
    case ion_ios_paperplane_outline = "ion-ios-paperplane-outline"
    case ion_ios_partlysunny = "ion-ios-partlysunny"
    case ion_ios_partlysunny_outline = "ion-ios-partlysunny-outline"
    case ion_ios_pause = "ion-ios-pause"
    case ion_ios_pause_outline = "ion-ios-pause-outline"
    case ion_ios_paw = "ion-ios-paw"
    case ion_ios_paw_outline = "ion-ios-paw-outline"
    case ion_ios_people = "ion-ios-people"
    case ion_ios_people_outline = "ion-ios-people-outline"
    case ion_ios_person = "ion-ios-person"
    case ion_ios_person_outline = "ion-ios-person-outline"
    case ion_ios_personadd = "ion-ios-personadd"
    case ion_ios_personadd_outline = "ion-ios-personadd-outline"
    case ion_ios_photos = "ion-ios-photos"
    case ion_ios_photos_outline = "ion-ios-photos-outline"
    case ion_ios_pie = "ion-ios-pie"
    case ion_ios_pie_outline = "ion-ios-pie-outline"
    case ion_ios_pint = "ion-ios-pint"
    case ion_ios_pint_outline = "ion-ios-pint-outline"
    case ion_ios_play = "ion-ios-play"
    case ion_ios_play_outline = "ion-ios-play-outline"
    case ion_ios_plus = "ion-ios-plus"
    case ion_ios_plus_empty = "ion-ios-plus-empty"
    case ion_ios_plus_outline = "ion-ios-plus-outline"
    case ion_ios_pricetag = "ion-ios-pricetag"
    case ion_ios_pricetag_outline = "ion-ios-pricetag-outline"
    case ion_ios_pricetags = "ion-ios-pricetags"
    case ion_ios_pricetags_outline = "ion-ios-pricetags-outline"
    case ion_ios_printer = "ion-ios-printer"
    case ion_ios_printer_outline = "ion-ios-printer-outline"
    case ion_ios_pulse = "ion-ios-pulse"
    case ion_ios_pulse_strong = "ion-ios-pulse-strong"
    case ion_ios_rainy = "ion-ios-rainy"
    case ion_ios_rainy_outline = "ion-ios-rainy-outline"
    case ion_ios_recording = "ion-ios-recording"
    case ion_ios_recording_outline = "ion-ios-recording-outline"
    case ion_ios_redo = "ion-ios-redo"
    case ion_ios_redo_outline = "ion-ios-redo-outline"
    case ion_ios_refresh = "ion-ios-refresh"
    case ion_ios_refresh_empty = "ion-ios-refresh-empty"
    case ion_ios_refresh_outline = "ion-ios-refresh-outline"
    case ion_ios_reload = "ion-ios-reload"
    case ion_ios_reverse_camera = "ion-ios-reverse-camera"
    case ion_ios_reverse_camera_outline = "ion-ios-reverse-camera-outline"
    case ion_ios_rewind = "ion-ios-rewind"
    case ion_ios_rewind_outline = "ion-ios-rewind-outline"
    case ion_ios_rose = "ion-ios-rose"
    case ion_ios_rose_outline = "ion-ios-rose-outline"
    case ion_ios_search = "ion-ios-search"
    case ion_ios_search_strong = "ion-ios-search-strong"
    case ion_ios_settings = "ion-ios-settings"
    case ion_ios_settings_strong = "ion-ios-settings-strong"
    case ion_ios_shuffle = "ion-ios-shuffle"
    case ion_ios_shuffle_strong = "ion-ios-shuffle-strong"
    case ion_ios_skipbackward = "ion-ios-skipbackward"
    case ion_ios_skipbackward_outline = "ion-ios-skipbackward-outline"
    case ion_ios_skipforward = "ion-ios-skipforward"
    case ion_ios_skipforward_outline = "ion-ios-skipforward-outline"
    case ion_ios_snowy = "ion-ios-snowy"
    case ion_ios_speedometer = "ion-ios-speedometer"
    case ion_ios_speedometer_outline = "ion-ios-speedometer-outline"
    case ion_ios_star = "ion-ios-star"
    case ion_ios_star_half = "ion-ios-star-half"
    case ion_ios_star_outline = "ion-ios-star-outline"
    case ion_ios_stopwatch = "ion-ios-stopwatch"
    case ion_ios_stopwatch_outline = "ion-ios-stopwatch-outline"
    case ion_ios_sunny = "ion-ios-sunny"
    case ion_ios_sunny_outline = "ion-ios-sunny-outline"
    case ion_ios_telephone = "ion-ios-telephone"
    case ion_ios_telephone_outline = "ion-ios-telephone-outline"
    case ion_ios_tennisball = "ion-ios-tennisball"
    case ion_ios_tennisball_outline = "ion-ios-tennisball-outline"
    case ion_ios_thunderstorm = "ion-ios-thunderstorm"
    case ion_ios_thunderstorm_outline = "ion-ios-thunderstorm-outline"
    case ion_ios_time = "ion-ios-time"
    case ion_ios_time_outline = "ion-ios-time-outline"
    case ion_ios_timer = "ion-ios-timer"
    case ion_ios_timer_outline = "ion-ios-timer-outline"
    case ion_ios_toggle = "ion-ios-toggle"
    case ion_ios_toggle_outline = "ion-ios-toggle-outline"
    case ion_ios_trash = "ion-ios-trash"
    case ion_ios_trash_outline = "ion-ios-trash-outline"
    case ion_ios_undo = "ion-ios-undo"
    case ion_ios_undo_outline = "ion-ios-undo-outline"
    case ion_ios_unlocked = "ion-ios-unlocked"
    case ion_ios_unlocked_outline = "ion-ios-unlocked-outline"
    case ion_ios_upload = "ion-ios-upload"
    case ion_ios_upload_outline = "ion-ios-upload-outline"
    case ion_ios_videocam = "ion-ios-videocam"
    case ion_ios_videocam_outline = "ion-ios-videocam-outline"
    case ion_ios_volume_high = "ion-ios-volume-high"
    case ion_ios_volume_low = "ion-ios-volume-low"
    case ion_ios_wineglass = "ion-ios-wineglass"
    case ion_ios_wineglass_outline = "ion-ios-wineglass-outline"
    case ion_ios_world = "ion-ios-world"
    case ion_ios_world_outline = "ion-ios-world-outline"
    case ion_ipad = "ion-ipad"
    case ion_iphone = "ion-iphone"
    case ion_ipod = "ion-ipod"
    case ion_jet = "ion-jet"
    case ion_key = "ion-key"
    case ion_knife = "ion-knife"
    case ion_laptop = "ion-laptop"
    case ion_leaf = "ion-leaf"
    case ion_levels = "ion-levels"
    case ion_lightbulb = "ion-lightbulb"
    case ion_link = "ion-link"
    case ion_load_a = "ion-load-a"
    case ion_load_b = "ion-load-b"
    case ion_load_c = "ion-load-c"
    case ion_load_d = "ion-load-d"
    case ion_location = "ion-location"
    case ion_lock_combination = "ion-lock-combination"
    case ion_locked = "ion-locked"
    case ion_log_in = "ion-log-in"
    case ion_log_out = "ion-log-out"
    case ion_loop = "ion-loop"
    case ion_magnet = "ion-magnet"
    case ion_male = "ion-male"
    case ion_man = "ion-man"
    case ion_map = "ion-map"
    case ion_medkit = "ion-medkit"
    case ion_merge = "ion-merge"
    case ion_mic_a = "ion-mic-a"
    case ion_mic_b = "ion-mic-b"
    case ion_mic_c = "ion-mic-c"
    case ion_minus = "ion-minus"
    case ion_minus_circled = "ion-minus-circled"
    case ion_minus_round = "ion-minus-round"
    case ion_model_s = "ion-model-s"
    case ion_monitor = "ion-monitor"
    case ion_more = "ion-more"
    case ion_mouse = "ion-mouse"
    case ion_music_note = "ion-music-note"
    case ion_navicon = "ion-navicon"
    case ion_navicon_round = "ion-navicon-round"
    case ion_navigate = "ion-navigate"
    case ion_network = "ion-network"
    case ion_no_smoking = "ion-no-smoking"
    case ion_nuclear = "ion-nuclear"
    case ion_outlet = "ion-outlet"
    case ion_paintbrush = "ion-paintbrush"
    case ion_paintbucket = "ion-paintbucket"
    case ion_paper_airplane = "ion-paper-airplane"
    case ion_paperclip = "ion-paperclip"
    case ion_pause = "ion-pause"
    case ion_person = "ion-person"
    case ion_person_add = "ion-person-add"
    case ion_person_stalker = "ion-person-stalker"
    case ion_pie_graph = "ion-pie-graph"
    case ion_pin = "ion-pin"
    case ion_pinpoint = "ion-pinpoint"
    case ion_pizza = "ion-pizza"
    case ion_plane = "ion-plane"
    case ion_planet = "ion-planet"
    case ion_play = "ion-play"
    case ion_playstation = "ion-playstation"
    case ion_plus = "ion-plus"
    case ion_plus_circled = "ion-plus-circled"
    case ion_plus_round = "ion-plus-round"
    case ion_podium = "ion-podium"
    case ion_pound = "ion-pound"
    case ion_power = "ion-power"
    case ion_pricetag = "ion-pricetag"
    case ion_pricetags = "ion-pricetags"
    case ion_printer = "ion-printer"
    case ion_pull_request = "ion-pull-request"
    case ion_qr_scanner = "ion-qr-scanner"
    case ion_quote = "ion-quote"
    case ion_radio_waves = "ion-radio-waves"
    case ion_record = "ion-record"
    case ion_refresh = "ion-refresh"
    case ion_reply = "ion-reply"
    case ion_reply_all = "ion-reply-all"
    case ion_ribbon_a = "ion-ribbon-a"
    case ion_ribbon_b = "ion-ribbon-b"
    case ion_sad = "ion-sad"
    case ion_sad_outline = "ion-sad-outline"
    case ion_scissors = "ion-scissors"
    case ion_search = "ion-search"
    case ion_settings = "ion-settings"
    case ion_share = "ion-share"
    case ion_shuffle = "ion-shuffle"
    case ion_skip_backward = "ion-skip-backward"
    case ion_skip_forward = "ion-skip-forward"
    case ion_social_android = "ion-social-android"
    case ion_social_android_outline = "ion-social-android-outline"
    case ion_social_angular = "ion-social-angular"
    case ion_social_angular_outline = "ion-social-angular-outline"
    case ion_social_apple = "ion-social-apple"
    case ion_social_apple_outline = "ion-social-apple-outline"
    case ion_social_bitcoin = "ion-social-bitcoin"
    case ion_social_bitcoin_outline = "ion-social-bitcoin-outline"
    case ion_social_buffer = "ion-social-buffer"
    case ion_social_buffer_outline = "ion-social-buffer-outline"
    case ion_social_chrome = "ion-social-chrome"
    case ion_social_chrome_outline = "ion-social-chrome-outline"
    case ion_social_codepen = "ion-social-codepen"
    case ion_social_codepen_outline = "ion-social-codepen-outline"
    case ion_social_css3 = "ion-social-css3"
    case ion_social_css3_outline = "ion-social-css3-outline"
    case ion_social_designernews = "ion-social-designernews"
    case ion_social_designernews_outline = "ion-social-designernews-outline"
    case ion_social_dribbble = "ion-social-dribbble"
    case ion_social_dribbble_outline = "ion-social-dribbble-outline"
    case ion_social_dropbox = "ion-social-dropbox"
    case ion_social_dropbox_outline = "ion-social-dropbox-outline"
    case ion_social_euro = "ion-social-euro"
    case ion_social_euro_outline = "ion-social-euro-outline"
    case ion_social_facebook = "ion-social-facebook"
    case ion_social_facebook_outline = "ion-social-facebook-outline"
    case ion_social_foursquare = "ion-social-foursquare"
    case ion_social_foursquare_outline = "ion-social-foursquare-outline"
    case ion_social_freebsd_devil = "ion-social-freebsd-devil"
    case ion_social_github = "ion-social-github"
    case ion_social_github_outline = "ion-social-github-outline"
    case ion_social_google = "ion-social-google"
    case ion_social_google_outline = "ion-social-google-outline"
    case ion_social_googleplus = "ion-social-googleplus"
    case ion_social_googleplus_outline = "ion-social-googleplus-outline"
    case ion_social_hackernews = "ion-social-hackernews"
    case ion_social_hackernews_outline = "ion-social-hackernews-outline"
    case ion_social_html5 = "ion-social-html5"
    case ion_social_html5_outline = "ion-social-html5-outline"
    case ion_social_instagram = "ion-social-instagram"
    case ion_social_instagram_outline = "ion-social-instagram-outline"
    case ion_social_javascript = "ion-social-javascript"
    case ion_social_javascript_outline = "ion-social-javascript-outline"
    case ion_social_linkedin = "ion-social-linkedin"
    case ion_social_linkedin_outline = "ion-social-linkedin-outline"
    case ion_social_markdown = "ion-social-markdown"
    case ion_social_nodejs = "ion-social-nodejs"
    case ion_social_octocat = "ion-social-octocat"
    case ion_social_pinterest = "ion-social-pinterest"
    case ion_social_pinterest_outline = "ion-social-pinterest-outline"
    case ion_social_python = "ion-social-python"
    case ion_social_reddit = "ion-social-reddit"
    case ion_social_reddit_outline = "ion-social-reddit-outline"
    case ion_social_rss = "ion-social-rss"
    case ion_social_rss_outline = "ion-social-rss-outline"
    case ion_social_sass = "ion-social-sass"
    case ion_social_skype = "ion-social-skype"
    case ion_social_skype_outline = "ion-social-skype-outline"
    case ion_social_snapchat = "ion-social-snapchat"
    case ion_social_snapchat_outline = "ion-social-snapchat-outline"
    case ion_social_tumblr = "ion-social-tumblr"
    case ion_social_tumblr_outline = "ion-social-tumblr-outline"
    case ion_social_tux = "ion-social-tux"
    case ion_social_twitch = "ion-social-twitch"
    case ion_social_twitch_outline = "ion-social-twitch-outline"
    case ion_social_twitter = "ion-social-twitter"
    case ion_social_twitter_outline = "ion-social-twitter-outline"
    case ion_social_usd = "ion-social-usd"
    case ion_social_usd_outline = "ion-social-usd-outline"
    case ion_social_vimeo = "ion-social-vimeo"
    case ion_social_vimeo_outline = "ion-social-vimeo-outline"
    case ion_social_whatsapp = "ion-social-whatsapp"
    case ion_social_whatsapp_outline = "ion-social-whatsapp-outline"
    case ion_social_windows = "ion-social-windows"
    case ion_social_windows_outline = "ion-social-windows-outline"
    case ion_social_wordpress = "ion-social-wordpress"
    case ion_social_wordpress_outline = "ion-social-wordpress-outline"
    case ion_social_yahoo = "ion-social-yahoo"
    case ion_social_yahoo_outline = "ion-social-yahoo-outline"
    case ion_social_yen = "ion-social-yen"
    case ion_social_yen_outline = "ion-social-yen-outline"
    case ion_social_youtube = "ion-social-youtube"
    case ion_social_youtube_outline = "ion-social-youtube-outline"
    case ion_soup_can = "ion-soup-can"
    case ion_soup_can_outline = "ion-soup-can-outline"
    case ion_speakerphone = "ion-speakerphone"
    case ion_speedometer = "ion-speedometer"
    case ion_spoon = "ion-spoon"
    case ion_star = "ion-star"
    case ion_stats_bars = "ion-stats-bars"
    case ion_steam = "ion-steam"
    case ion_stop = "ion-stop"
    case ion_thermometer = "ion-thermometer"
    case ion_thumbsdown = "ion-thumbsdown"
    case ion_thumbsup = "ion-thumbsup"
    case ion_toggle = "ion-toggle"
    case ion_toggle_filled = "ion-toggle-filled"
    case ion_transgender = "ion-transgender"
    case ion_trash_a = "ion-trash-a"
    case ion_trash_b = "ion-trash-b"
    case ion_trophy = "ion-trophy"
    case ion_tshirt = "ion-tshirt"
    case ion_tshirt_outline = "ion-tshirt-outline"
    case ion_umbrella = "ion-umbrella"
    case ion_university = "ion-university"
    case ion_unlocked = "ion-unlocked"
    case ion_upload = "ion-upload"
    case ion_usb = "ion-usb"
    case ion_videocamera = "ion-videocamera"
    case ion_volume_high = "ion-volume-high"
    case ion_volume_low = "ion-volume-low"
    case ion_volume_medium = "ion-volume-medium"
    case ion_volume_mute = "ion-volume-mute"
    case ion_wand = "ion-wand"
    case ion_waterdrop = "ion-waterdrop"
    case ion_wifi = "ion-wifi"
    case ion_wineglass = "ion-wineglass"
    case ion_woman = "ion-woman"
    case ion_wrench = "ion-wrench"
    case ion_xbox = "ion-xbox"
}

public let ioniconsCodes: [String: String] = [
    "ion-alert": "\u{f101}",
    "ion-alert-circled": "\u{f100}",
    "ion-android-add": "\u{f2c7}",
    "ion-android-add-circle": "\u{f359}",
    "ion-android-alarm-clock": "\u{f35a}",
    "ion-android-alert": "\u{f35b}",
    "ion-android-apps": "\u{f35c}",
    "ion-android-archive": "\u{f2c9}",
    "ion-android-arrow-back": "\u{f2ca}",
    "ion-android-arrow-down": "\u{f35d}",
    "ion-android-arrow-dropdown": "\u{f35f}",
    "ion-android-arrow-dropdown-circle": "\u{f35e}",
    "ion-android-arrow-dropleft": "\u{f361}",
    "ion-android-arrow-dropleft-circle": "\u{f360}",
    "ion-android-arrow-dropright": "\u{f363}",
    "ion-android-arrow-dropright-circle": "\u{f362}",
    "ion-android-arrow-dropup": "\u{f365}",
    "ion-android-arrow-dropup-circle": "\u{f364}",
    "ion-android-arrow-forward": "\u{f30f}",
    "ion-android-arrow-up": "\u{f366}",
    "ion-android-attach": "\u{f367}",
    "ion-android-bar": "\u{f368}",
    "ion-android-bicycle": "\u{f369}",
    "ion-android-boat": "\u{f36a}",
    "ion-android-bookmark": "\u{f36b}",
    "ion-android-bulb": "\u{f36c}",
    "ion-android-bus": "\u{f36d}",
    "ion-android-calendar": "\u{f2d1}",
    "ion-android-call": "\u{f2d2}",
    "ion-android-camera": "\u{f2d3}",
    "ion-android-cancel": "\u{f36e}",
    "ion-android-car": "\u{f36f}",
    "ion-android-cart": "\u{f370}",
    "ion-android-chat": "\u{f2d4}",
    "ion-android-checkbox": "\u{f374}",
    "ion-android-checkbox-blank": "\u{f371}",
    "ion-android-checkbox-outline": "\u{f373}",
    "ion-android-checkbox-outline-blank": "\u{f372}",
    "ion-android-checkmark-circle": "\u{f375}",
    "ion-android-clipboard": "\u{f376}",
    "ion-android-close": "\u{f2d7}",
    "ion-android-cloud": "\u{f37a}",
    "ion-android-cloud-circle": "\u{f377}",
    "ion-android-cloud-done": "\u{f378}",
    "ion-android-cloud-outline": "\u{f379}",
    "ion-android-color-palette": "\u{f37b}",
    "ion-android-compass": "\u{f37c}",
    "ion-android-contact": "\u{f2d8}",
    "ion-android-contacts": "\u{f2d9}",
    "ion-android-contract": "\u{f37d}",
    "ion-android-create": "\u{f37e}",
    "ion-android-delete": "\u{f37f}",
    "ion-android-desktop": "\u{f380}",
    "ion-android-document": "\u{f381}",
    "ion-android-done": "\u{f383}",
    "ion-android-done-all": "\u{f382}",
    "ion-android-download": "\u{f2dd}",
    "ion-android-drafts": "\u{f384}",
    "ion-android-exit": "\u{f385}",
    "ion-android-expand": "\u{f386}",
    "ion-android-favorite": "\u{f388}",
    "ion-android-favorite-outline": "\u{f387}",
    "ion-android-film": "\u{f389}",
    "ion-android-folder": "\u{f2e0}",
    "ion-android-folder-open": "\u{f38a}",
    "ion-android-funnel": "\u{f38b}",
    "ion-android-globe": "\u{f38c}",
    "ion-android-hand": "\u{f2e3}",
    "ion-android-hangout": "\u{f38d}",
    "ion-android-happy": "\u{f38e}",
    "ion-android-home": "\u{f38f}",
    "ion-android-image": "\u{f2e4}",
    "ion-android-laptop": "\u{f390}",
    "ion-android-list": "\u{f391}",
    "ion-android-locate": "\u{f2e9}",
    "ion-android-lock": "\u{f392}",
    "ion-android-mail": "\u{f2eb}",
    "ion-android-map": "\u{f393}",
    "ion-android-menu": "\u{f394}",
    "ion-android-microphone": "\u{f2ec}",
    "ion-android-microphone-off": "\u{f395}",
    "ion-android-more-horizontal": "\u{f396}",
    "ion-android-more-vertical": "\u{f397}",
    "ion-android-navigate": "\u{f398}",
    "ion-android-notifications": "\u{f39b}",
    "ion-android-notifications-none": "\u{f399}",
    "ion-android-notifications-off": "\u{f39a}",
    "ion-android-open": "\u{f39c}",
    "ion-android-options": "\u{f39d}",
    "ion-android-people": "\u{f39e}",
    "ion-android-person": "\u{f3a0}",
    "ion-android-person-add": "\u{f39f}",
    "ion-android-phone-landscape": "\u{f3a1}",
    "ion-android-phone-portrait": "\u{f3a2}",
    "ion-android-pin": "\u{f3a3}",
    "ion-android-plane": "\u{f3a4}",
    "ion-android-playstore": "\u{f2f0}",
    "ion-android-print": "\u{f3a5}",
    "ion-android-radio-button-off": "\u{f3a6}",
    "ion-android-radio-button-on": "\u{f3a7}",
    "ion-android-refresh": "\u{f3a8}",
    "ion-android-remove": "\u{f2f4}",
    "ion-android-remove-circle": "\u{f3a9}",
    "ion-android-restaurant": "\u{f3aa}",
    "ion-android-sad": "\u{f3ab}",
    "ion-android-search": "\u{f2f5}",
    "ion-android-send": "\u{f2f6}",
    "ion-android-settings": "\u{f2f7}",
    "ion-android-share": "\u{f2f8}",
    "ion-android-share-alt": "\u{f3ac}",
    "ion-android-star": "\u{f2fc}",
    "ion-android-star-half": "\u{f3ad}",
    "ion-android-star-outline": "\u{f3ae}",
    "ion-android-stopwatch": "\u{f2fd}",
    "ion-android-subway": "\u{f3af}",
    "ion-android-sunny": "\u{f3b0}",
    "ion-android-sync": "\u{f3b1}",
    "ion-android-textsms": "\u{f3b2}",
    "ion-android-time": "\u{f3b3}",
    "ion-android-train": "\u{f3b4}",
    "ion-android-unlock": "\u{f3b5}",
    "ion-android-upload": "\u{f3b6}",
    "ion-android-volume-down": "\u{f3b7}",
    "ion-android-volume-mute": "\u{f3b8}",
    "ion-android-volume-off": "\u{f3b9}",
    "ion-android-volume-up": "\u{f3ba}",
    "ion-android-walk": "\u{f3bb}",
    "ion-android-warning": "\u{f3bc}",
    "ion-android-watch": "\u{f3bd}",
    "ion-android-wifi": "\u{f305}",
    "ion-aperture": "\u{f313}",
    "ion-archive": "\u{f102}",
    "ion-arrow-down-a": "\u{f103}",
    "ion-arrow-down-b": "\u{f104}",
    "ion-arrow-down-c": "\u{f105}",
    "ion-arrow-expand": "\u{f25e}",
    "ion-arrow-graph-down-left": "\u{f25f}",
    "ion-arrow-graph-down-right": "\u{f260}",
    "ion-arrow-graph-up-left": "\u{f261}",
    "ion-arrow-graph-up-right": "\u{f262}",
    "ion-arrow-left-a": "\u{f106}",
    "ion-arrow-left-b": "\u{f107}",
    "ion-arrow-left-c": "\u{f108}",
    "ion-arrow-move": "\u{f263}",
    "ion-arrow-resize": "\u{f264}",
    "ion-arrow-return-left": "\u{f265}",
    "ion-arrow-return-right": "\u{f266}",
    "ion-arrow-right-a": "\u{f109}",
    "ion-arrow-right-b": "\u{f10a}",
    "ion-arrow-right-c": "\u{f10b}",
    "ion-arrow-shrink": "\u{f267}",
    "ion-arrow-swap": "\u{f268}",
    "ion-arrow-up-a": "\u{f10c}",
    "ion-arrow-up-b": "\u{f10d}",
    "ion-arrow-up-c": "\u{f10e}",
    "ion-asterisk": "\u{f314}",
    "ion-at": "\u{f10f}",
    "ion-backspace": "\u{f3bf}",
    "ion-backspace-outline": "\u{f3be}",
    "ion-bag": "\u{f110}",
    "ion-battery-charging": "\u{f111}",
    "ion-battery-empty": "\u{f112}",
    "ion-battery-full": "\u{f113}",
    "ion-battery-half": "\u{f114}",
    "ion-battery-low": "\u{f115}",
    "ion-beaker": "\u{f269}",
    "ion-beer": "\u{f26a}",
    "ion-bluetooth": "\u{f116}",
    "ion-bonfire": "\u{f315}",
    "ion-bookmark": "\u{f26b}",
    "ion-bowtie": "\u{f3c0}",
    "ion-briefcase": "\u{f26c}",
    "ion-bug": "\u{f2be}",
    "ion-calculator": "\u{f26d}",
    "ion-calendar": "\u{f117}",
    "ion-camera": "\u{f118}",
    "ion-card": "\u{f119}",
    "ion-cash": "\u{f316}",
    "ion-chatbox": "\u{f11b}",
    "ion-chatbox-working": "\u{f11a}",
    "ion-chatboxes": "\u{f11c}",
    "ion-chatbubble": "\u{f11e}",
    "ion-chatbubble-working": "\u{f11d}",
    "ion-chatbubbles": "\u{f11f}",
    "ion-checkmark": "\u{f122}",
    "ion-checkmark-circled": "\u{f120}",
    "ion-checkmark-round": "\u{f121}",
    "ion-chevron-down": "\u{f123}",
    "ion-chevron-left": "\u{f124}",
    "ion-chevron-right": "\u{f125}",
    "ion-chevron-up": "\u{f126}",
    "ion-clipboard": "\u{f127}",
    "ion-clock": "\u{f26e}",
    "ion-close": "\u{f12a}",
    "ion-close-circled": "\u{f128}",
    "ion-close-round": "\u{f129}",
    "ion-closed-captioning": "\u{f317}",
    "ion-cloud": "\u{f12b}",
    "ion-code": "\u{f271}",
    "ion-code-download": "\u{f26f}",
    "ion-code-working": "\u{f270}",
    "ion-coffee": "\u{f272}",
    "ion-compass": "\u{f273}",
    "ion-compose": "\u{f12c}",
    "ion-connection-bars": "\u{f274}",
    "ion-contrast": "\u{f275}",
    "ion-crop": "\u{f3c1}",
    "ion-cube": "\u{f318}",
    "ion-disc": "\u{f12d}",
    "ion-document": "\u{f12f}",
    "ion-document-text": "\u{f12e}",
    "ion-drag": "\u{f130}",
    "ion-earth": "\u{f276}",
    "ion-easel": "\u{f3c2}",
    "ion-edit": "\u{f2bf}",
    "ion-egg": "\u{f277}",
    "ion-eject": "\u{f131}",
    "ion-email": "\u{f132}",
    "ion-email-unread": "\u{f3c3}",
    "ion-erlenmeyer-flask": "\u{f3c5}",
    "ion-erlenmeyer-flask-bubbles": "\u{f3c4}",
    "ion-eye": "\u{f133}",
    "ion-eye-disabled": "\u{f306}",
    "ion-female": "\u{f278}",
    "ion-filing": "\u{f134}",
    "ion-film-marker": "\u{f135}",
    "ion-fireball": "\u{f319}",
    "ion-flag": "\u{f279}",
    "ion-flame": "\u{f31a}",
    "ion-flash": "\u{f137}",
    "ion-flash-off": "\u{f136}",
    "ion-folder": "\u{f139}",
    "ion-fork": "\u{f27a}",
    "ion-fork-repo": "\u{f2c0}",
    "ion-forward": "\u{f13a}",
    "ion-funnel": "\u{f31b}",
    "ion-gear-a": "\u{f13d}",
    "ion-gear-b": "\u{f13e}",
    "ion-grid": "\u{f13f}",
    "ion-hammer": "\u{f27b}",
    "ion-happy": "\u{f31c}",
    "ion-happy-outline": "\u{f3c6}",
    "ion-headphone": "\u{f140}",
    "ion-heart": "\u{f141}",
    "ion-heart-broken": "\u{f31d}",
    "ion-help": "\u{f143}",
    "ion-help-buoy": "\u{f27c}",
    "ion-help-circled": "\u{f142}",
    "ion-home": "\u{f144}",
    "ion-icecream": "\u{f27d}",
    "ion-image": "\u{f147}",
    "ion-images": "\u{f148}",
    "ion-information": "\u{f14a}",
    "ion-information-circled": "\u{f149}",
    "ion-ionic": "\u{f14b}",
    "ion-ios-alarm": "\u{f3c8}",
    "ion-ios-alarm-outline": "\u{f3c7}",
    "ion-ios-albums": "\u{f3ca}",
    "ion-ios-albums-outline": "\u{f3c9}",
    "ion-ios-americanfootball": "\u{f3cc}",
    "ion-ios-americanfootball-outline": "\u{f3cb}",
    "ion-ios-analytics": "\u{f3ce}",
    "ion-ios-analytics-outline": "\u{f3cd}",
    "ion-ios-arrow-back": "\u{f3cf}",
    "ion-ios-arrow-down": "\u{f3d0}",
    "ion-ios-arrow-forward": "\u{f3d1}",
    "ion-ios-arrow-left": "\u{f3d2}",
    "ion-ios-arrow-right": "\u{f3d3}",
    "ion-ios-arrow-thin-down": "\u{f3d4}",
    "ion-ios-arrow-thin-left": "\u{f3d5}",
    "ion-ios-arrow-thin-right": "\u{f3d6}",
    "ion-ios-arrow-thin-up": "\u{f3d7}",
    "ion-ios-arrow-up": "\u{f3d8}",
    "ion-ios-at": "\u{f3da}",
    "ion-ios-at-outline": "\u{f3d9}",
    "ion-ios-barcode": "\u{f3dc}",
    "ion-ios-barcode-outline": "\u{f3db}",
    "ion-ios-baseball": "\u{f3de}",
    "ion-ios-baseball-outline": "\u{f3dd}",
    "ion-ios-basketball": "\u{f3e0}",
    "ion-ios-basketball-outline": "\u{f3df}",
    "ion-ios-bell": "\u{f3e2}",
    "ion-ios-bell-outline": "\u{f3e1}",
    "ion-ios-body": "\u{f3e4}",
    "ion-ios-body-outline": "\u{f3e3}",
    "ion-ios-bolt": "\u{f3e6}",
    "ion-ios-bolt-outline": "\u{f3e5}",
    "ion-ios-book": "\u{f3e8}",
    "ion-ios-book-outline": "\u{f3e7}",
    "ion-ios-bookmarks": "\u{f3ea}",
    "ion-ios-bookmarks-outline": "\u{f3e9}",
    "ion-ios-box": "\u{f3ec}",
    "ion-ios-box-outline": "\u{f3eb}",
    "ion-ios-briefcase": "\u{f3ee}",
    "ion-ios-briefcase-outline": "\u{f3ed}",
    "ion-ios-browsers": "\u{f3f0}",
    "ion-ios-browsers-outline": "\u{f3ef}",
    "ion-ios-calculator": "\u{f3f2}",
    "ion-ios-calculator-outline": "\u{f3f1}",
    "ion-ios-calendar": "\u{f3f4}",
    "ion-ios-calendar-outline": "\u{f3f3}",
    "ion-ios-camera": "\u{f3f6}",
    "ion-ios-camera-outline": "\u{f3f5}",
    "ion-ios-cart": "\u{f3f8}",
    "ion-ios-cart-outline": "\u{f3f7}",
    "ion-ios-chatboxes": "\u{f3fa}",
    "ion-ios-chatboxes-outline": "\u{f3f9}",
    "ion-ios-chatbubble": "\u{f3fc}",
    "ion-ios-chatbubble-outline": "\u{f3fb}",
    "ion-ios-checkmark": "\u{f3ff}",
    "ion-ios-checkmark-empty": "\u{f3fd}",
    "ion-ios-checkmark-outline": "\u{f3fe}",
    "ion-ios-circle-filled": "\u{f400}",
    "ion-ios-circle-outline": "\u{f401}",
    "ion-ios-clock": "\u{f403}",
    "ion-ios-clock-outline": "\u{f402}",
    "ion-ios-close": "\u{f406}",
    "ion-ios-close-empty": "\u{f404}",
    "ion-ios-close-outline": "\u{f405}",
    "ion-ios-cloud": "\u{f40c}",
    "ion-ios-cloud-download": "\u{f408}",
    "ion-ios-cloud-download-outline": "\u{f407}",
    "ion-ios-cloud-outline": "\u{f409}",
    "ion-ios-cloud-upload": "\u{f40b}",
    "ion-ios-cloud-upload-outline": "\u{f40a}",
    "ion-ios-cloudy": "\u{f410}",
    "ion-ios-cloudy-night": "\u{f40e}",
    "ion-ios-cloudy-night-outline": "\u{f40d}",
    "ion-ios-cloudy-outline": "\u{f40f}",
    "ion-ios-cog": "\u{f412}",
    "ion-ios-cog-outline": "\u{f411}",
    "ion-ios-color-filter": "\u{f414}",
    "ion-ios-color-filter-outline": "\u{f413}",
    "ion-ios-color-wand": "\u{f416}",
    "ion-ios-color-wand-outline": "\u{f415}",
    "ion-ios-compose": "\u{f418}",
    "ion-ios-compose-outline": "\u{f417}",
    "ion-ios-contact": "\u{f41a}",
    "ion-ios-contact-outline": "\u{f419}",
    "ion-ios-copy": "\u{f41c}",
    "ion-ios-copy-outline": "\u{f41b}",
    "ion-ios-crop": "\u{f41e}",
    "ion-ios-crop-strong": "\u{f41d}",
    "ion-ios-download": "\u{f420}",
    "ion-ios-download-outline": "\u{f41f}",
    "ion-ios-drag": "\u{f421}",
    "ion-ios-email": "\u{f423}",
    "ion-ios-email-outline": "\u{f422}",
    "ion-ios-eye": "\u{f425}",
    "ion-ios-eye-outline": "\u{f424}",
    "ion-ios-fastforward": "\u{f427}",
    "ion-ios-fastforward-outline": "\u{f426}",
    "ion-ios-filing": "\u{f429}",
    "ion-ios-filing-outline": "\u{f428}",
    "ion-ios-film": "\u{f42b}",
    "ion-ios-film-outline": "\u{f42a}",
    "ion-ios-flag": "\u{f42d}",
    "ion-ios-flag-outline": "\u{f42c}",
    "ion-ios-flame": "\u{f42f}",
    "ion-ios-flame-outline": "\u{f42e}",
    "ion-ios-flask": "\u{f431}",
    "ion-ios-flask-outline": "\u{f430}",
    "ion-ios-flower": "\u{f433}",
    "ion-ios-flower-outline": "\u{f432}",
    "ion-ios-folder": "\u{f435}",
    "ion-ios-folder-outline": "\u{f434}",
    "ion-ios-football": "\u{f437}",
    "ion-ios-football-outline": "\u{f436}",
    "ion-ios-game-controller-a": "\u{f439}",
    "ion-ios-game-controller-a-outline": "\u{f438}",
    "ion-ios-game-controller-b": "\u{f43b}",
    "ion-ios-game-controller-b-outline": "\u{f43a}",
    "ion-ios-gear": "\u{f43d}",
    "ion-ios-gear-outline": "\u{f43c}",
    "ion-ios-glasses": "\u{f43f}",
    "ion-ios-glasses-outline": "\u{f43e}",
    "ion-ios-grid-view": "\u{f441}",
    "ion-ios-grid-view-outline": "\u{f440}",
    "ion-ios-heart": "\u{f443}",
    "ion-ios-heart-outline": "\u{f442}",
    "ion-ios-help": "\u{f446}",
    "ion-ios-help-empty": "\u{f444}",
    "ion-ios-help-outline": "\u{f445}",
    "ion-ios-home": "\u{f448}",
    "ion-ios-home-outline": "\u{f447}",
    "ion-ios-infinite": "\u{f44a}",
    "ion-ios-infinite-outline": "\u{f449}",
    "ion-ios-information": "\u{f44d}",
    "ion-ios-information-empty": "\u{f44b}",
    "ion-ios-information-outline": "\u{f44c}",
    "ion-ios-ionic-outline": "\u{f44e}",
    "ion-ios-keypad": "\u{f450}",
    "ion-ios-keypad-outline": "\u{f44f}",
    "ion-ios-lightbulb": "\u{f452}",
    "ion-ios-lightbulb-outline": "\u{f451}",
    "ion-ios-list": "\u{f454}",
    "ion-ios-list-outline": "\u{f453}",
    "ion-ios-location": "\u{f456}",
    "ion-ios-location-outline": "\u{f455}",
    "ion-ios-locked": "\u{f458}",
    "ion-ios-locked-outline": "\u{f457}",
    "ion-ios-loop": "\u{f45a}",
    "ion-ios-loop-strong": "\u{f459}",
    "ion-ios-medical": "\u{f45c}",
    "ion-ios-medical-outline": "\u{f45b}",
    "ion-ios-medkit": "\u{f45e}",
    "ion-ios-medkit-outline": "\u{f45d}",
    "ion-ios-mic": "\u{f461}",
    "ion-ios-mic-off": "\u{f45f}",
    "ion-ios-mic-outline": "\u{f460}",
    "ion-ios-minus": "\u{f464}",
    "ion-ios-minus-empty": "\u{f462}",
    "ion-ios-minus-outline": "\u{f463}",
    "ion-ios-monitor": "\u{f466}",
    "ion-ios-monitor-outline": "\u{f465}",
    "ion-ios-moon": "\u{f468}",
    "ion-ios-moon-outline": "\u{f467}",
    "ion-ios-more": "\u{f46a}",
    "ion-ios-more-outline": "\u{f469}",
    "ion-ios-musical-note": "\u{f46b}",
    "ion-ios-musical-notes": "\u{f46c}",
    "ion-ios-navigate": "\u{f46e}",
    "ion-ios-navigate-outline": "\u{f46d}",
    "ion-ios-nutrition": "\u{f470}",
    "ion-ios-nutrition-outline": "\u{f46f}",
    "ion-ios-paper": "\u{f472}",
    "ion-ios-paper-outline": "\u{f471}",
    "ion-ios-paperplane": "\u{f474}",
    "ion-ios-paperplane-outline": "\u{f473}",
    "ion-ios-partlysunny": "\u{f476}",
    "ion-ios-partlysunny-outline": "\u{f475}",
    "ion-ios-pause": "\u{f478}",
    "ion-ios-pause-outline": "\u{f477}",
    "ion-ios-paw": "\u{f47a}",
    "ion-ios-paw-outline": "\u{f479}",
    "ion-ios-people": "\u{f47c}",
    "ion-ios-people-outline": "\u{f47b}",
    "ion-ios-person": "\u{f47e}",
    "ion-ios-person-outline": "\u{f47d}",
    "ion-ios-personadd": "\u{f480}",
    "ion-ios-personadd-outline": "\u{f47f}",
    "ion-ios-photos": "\u{f482}",
    "ion-ios-photos-outline": "\u{f481}",
    "ion-ios-pie": "\u{f484}",
    "ion-ios-pie-outline": "\u{f483}",
    "ion-ios-pint": "\u{f486}",
    "ion-ios-pint-outline": "\u{f485}",
    "ion-ios-play": "\u{f488}",
    "ion-ios-play-outline": "\u{f487}",
    "ion-ios-plus": "\u{f48b}",
    "ion-ios-plus-empty": "\u{f489}",
    "ion-ios-plus-outline": "\u{f48a}",
    "ion-ios-pricetag": "\u{f48d}",
    "ion-ios-pricetag-outline": "\u{f48c}",
    "ion-ios-pricetags": "\u{f48f}",
    "ion-ios-pricetags-outline": "\u{f48e}",
    "ion-ios-printer": "\u{f491}",
    "ion-ios-printer-outline": "\u{f490}",
    "ion-ios-pulse": "\u{f493}",
    "ion-ios-pulse-strong": "\u{f492}",
    "ion-ios-rainy": "\u{f495}",
    "ion-ios-rainy-outline": "\u{f494}",
    "ion-ios-recording": "\u{f497}",
    "ion-ios-recording-outline": "\u{f496}",
    "ion-ios-redo": "\u{f499}",
    "ion-ios-redo-outline": "\u{f498}",
    "ion-ios-refresh": "\u{f49c}",
    "ion-ios-refresh-empty": "\u{f49a}",
    "ion-ios-refresh-outline": "\u{f49b}",
    "ion-ios-reload": "\u{f49d}",
    "ion-ios-reverse-camera": "\u{f49f}",
    "ion-ios-reverse-camera-outline": "\u{f49e}",
    "ion-ios-rewind": "\u{f4a1}",
    "ion-ios-rewind-outline": "\u{f4a0}",
    "ion-ios-rose": "\u{f4a3}",
    "ion-ios-rose-outline": "\u{f4a2}",
    "ion-ios-search": "\u{f4a5}",
    "ion-ios-search-strong": "\u{f4a4}",
    "ion-ios-settings": "\u{f4a7}",
    "ion-ios-settings-strong": "\u{f4a6}",
    "ion-ios-shuffle": "\u{f4a9}",
    "ion-ios-shuffle-strong": "\u{f4a8}",
    "ion-ios-skipbackward": "\u{f4ab}",
    "ion-ios-skipbackward-outline": "\u{f4aa}",
    "ion-ios-skipforward": "\u{f4ad}",
    "ion-ios-skipforward-outline": "\u{f4ac}",
    "ion-ios-snowy": "\u{f4ae}",
    "ion-ios-speedometer": "\u{f4b0}",
    "ion-ios-speedometer-outline": "\u{f4af}",
    "ion-ios-star": "\u{f4b3}",
    "ion-ios-star-half": "\u{f4b1}",
    "ion-ios-star-outline": "\u{f4b2}",
    "ion-ios-stopwatch": "\u{f4b5}",
    "ion-ios-stopwatch-outline": "\u{f4b4}",
    "ion-ios-sunny": "\u{f4b7}",
    "ion-ios-sunny-outline": "\u{f4b6}",
    "ion-ios-telephone": "\u{f4b9}",
    "ion-ios-telephone-outline": "\u{f4b8}",
    "ion-ios-tennisball": "\u{f4bb}",
    "ion-ios-tennisball-outline": "\u{f4ba}",
    "ion-ios-thunderstorm": "\u{f4bd}",
    "ion-ios-thunderstorm-outline": "\u{f4bc}",
    "ion-ios-time": "\u{f4bf}",
    "ion-ios-time-outline": "\u{f4be}",
    "ion-ios-timer": "\u{f4c1}",
    "ion-ios-timer-outline": "\u{f4c0}",
    "ion-ios-toggle": "\u{f4c3}",
    "ion-ios-toggle-outline": "\u{f4c2}",
    "ion-ios-trash": "\u{f4c5}",
    "ion-ios-trash-outline": "\u{f4c4}",
    "ion-ios-undo": "\u{f4c7}",
    "ion-ios-undo-outline": "\u{f4c6}",
    "ion-ios-unlocked": "\u{f4c9}",
    "ion-ios-unlocked-outline": "\u{f4c8}",
    "ion-ios-upload": "\u{f4cb}",
    "ion-ios-upload-outline": "\u{f4ca}",
    "ion-ios-videocam": "\u{f4cd}",
    "ion-ios-videocam-outline": "\u{f4cc}",
    "ion-ios-volume-high": "\u{f4ce}",
    "ion-ios-volume-low": "\u{f4cf}",
    "ion-ios-wineglass": "\u{f4d1}",
    "ion-ios-wineglass-outline": "\u{f4d0}",
    "ion-ios-world": "\u{f4d3}",
    "ion-ios-world-outline": "\u{f4d2}",
    "ion-ipad": "\u{f1f9}",
    "ion-iphone": "\u{f1fa}",
    "ion-ipod": "\u{f1fb}",
    "ion-jet": "\u{f295}",
    "ion-key": "\u{f296}",
    "ion-knife": "\u{f297}",
    "ion-laptop": "\u{f1fc}",
    "ion-leaf": "\u{f1fd}",
    "ion-levels": "\u{f298}",
    "ion-lightbulb": "\u{f299}",
    "ion-link": "\u{f1fe}",
    "ion-load-a": "\u{f29a}",
    "ion-load-b": "\u{f29b}",
    "ion-load-c": "\u{f29c}",
    "ion-load-d": "\u{f29d}",
    "ion-location": "\u{f1ff}",
    "ion-lock-combination": "\u{f4d4}",
    "ion-locked": "\u{f200}",
    "ion-log-in": "\u{f29e}",
    "ion-log-out": "\u{f29f}",
    "ion-loop": "\u{f201}",
    "ion-magnet": "\u{f2a0}",
    "ion-male": "\u{f2a1}",
    "ion-man": "\u{f202}",
    "ion-map": "\u{f203}",
    "ion-medkit": "\u{f2a2}",
    "ion-merge": "\u{f33f}",
    "ion-mic-a": "\u{f204}",
    "ion-mic-b": "\u{f205}",
    "ion-mic-c": "\u{f206}",
    "ion-minus": "\u{f209}",
    "ion-minus-circled": "\u{f207}",
    "ion-minus-round": "\u{f208}",
    "ion-model-s": "\u{f2c1}",
    "ion-monitor": "\u{f20a}",
    "ion-more": "\u{f20b}",
    "ion-mouse": "\u{f340}",
    "ion-music-note": "\u{f20c}",
    "ion-navicon": "\u{f20e}",
    "ion-navicon-round": "\u{f20d}",
    "ion-navigate": "\u{f2a3}",
    "ion-network": "\u{f341}",
    "ion-no-smoking": "\u{f2c2}",
    "ion-nuclear": "\u{f2a4}",
    "ion-outlet": "\u{f342}",
    "ion-paintbrush": "\u{f4d5}",
    "ion-paintbucket": "\u{f4d6}",
    "ion-paper-airplane": "\u{f2c3}",
    "ion-paperclip": "\u{f20f}",
    "ion-pause": "\u{f210}",
    "ion-person": "\u{f213}",
    "ion-person-add": "\u{f211}",
    "ion-person-stalker": "\u{f212}",
    "ion-pie-graph": "\u{f2a5}",
    "ion-pin": "\u{f2a6}",
    "ion-pinpoint": "\u{f2a7}",
    "ion-pizza": "\u{f2a8}",
    "ion-plane": "\u{f214}",
    "ion-planet": "\u{f343}",
    "ion-play": "\u{f215}",
    "ion-playstation": "\u{f30a}",
    "ion-plus": "\u{f218}",
    "ion-plus-circled": "\u{f216}",
    "ion-plus-round": "\u{f217}",
    "ion-podium": "\u{f344}",
    "ion-pound": "\u{f219}",
    "ion-power": "\u{f2a9}",
    "ion-pricetag": "\u{f2aa}",
    "ion-pricetags": "\u{f2ab}",
    "ion-printer": "\u{f21a}",
    "ion-pull-request": "\u{f345}",
    "ion-qr-scanner": "\u{f346}",
    "ion-quote": "\u{f347}",
    "ion-radio-waves": "\u{f2ac}",
    "ion-record": "\u{f21b}",
    "ion-refresh": "\u{f21c}",
    "ion-reply": "\u{f21e}",
    "ion-reply-all": "\u{f21d}",
    "ion-ribbon-a": "\u{f348}",
    "ion-ribbon-b": "\u{f349}",
    "ion-sad": "\u{f34a}",
    "ion-sad-outline": "\u{f4d7}",
    "ion-scissors": "\u{f34b}",
    "ion-search": "\u{f21f}",
    "ion-settings": "\u{f2ad}",
    "ion-share": "\u{f220}",
    "ion-shuffle": "\u{f221}",
    "ion-skip-backward": "\u{f222}",
    "ion-skip-forward": "\u{f223}",
    "ion-social-android": "\u{f225}",
    "ion-social-android-outline": "\u{f224}",
    "ion-social-angular": "\u{f4d9}",
    "ion-social-angular-outline": "\u{f4d8}",
    "ion-social-apple": "\u{f227}",
    "ion-social-apple-outline": "\u{f226}",
    "ion-social-bitcoin": "\u{f2af}",
    "ion-social-bitcoin-outline": "\u{f2ae}",
    "ion-social-buffer": "\u{f229}",
    "ion-social-buffer-outline": "\u{f228}",
    "ion-social-chrome": "\u{f4db}",
    "ion-social-chrome-outline": "\u{f4da}",
    "ion-social-codepen": "\u{f4dd}",
    "ion-social-codepen-outline": "\u{f4dc}",
    "ion-social-css3": "\u{f4df}",
    "ion-social-css3-outline": "\u{f4de}",
    "ion-social-designernews": "\u{f22b}",
    "ion-social-designernews-outline": "\u{f22a}",
    "ion-social-dribbble": "\u{f22d}",
    "ion-social-dribbble-outline": "\u{f22c}",
    "ion-social-dropbox": "\u{f22f}",
    "ion-social-dropbox-outline": "\u{f22e}",
    "ion-social-euro": "\u{f4e1}",
    "ion-social-euro-outline": "\u{f4e0}",
    "ion-social-facebook": "\u{f231}",
    "ion-social-facebook-outline": "\u{f230}",
    "ion-social-foursquare": "\u{f34d}",
    "ion-social-foursquare-outline": "\u{f34c}",
    "ion-social-freebsd-devil": "\u{f2c4}",
    "ion-social-github": "\u{f233}",
    "ion-social-github-outline": "\u{f232}",
    "ion-social-google": "\u{f34f}",
    "ion-social-google-outline": "\u{f34e}",
    "ion-social-googleplus": "\u{f235}",
    "ion-social-googleplus-outline": "\u{f234}",
    "ion-social-hackernews": "\u{f237}",
    "ion-social-hackernews-outline": "\u{f236}",
    "ion-social-html5": "\u{f4e3}",
    "ion-social-html5-outline": "\u{f4e2}",
    "ion-social-instagram": "\u{f351}",
    "ion-social-instagram-outline": "\u{f350}",
    "ion-social-javascript": "\u{f4e5}",
    "ion-social-javascript-outline": "\u{f4e4}",
    "ion-social-linkedin": "\u{f239}",
    "ion-social-linkedin-outline": "\u{f238}",
    "ion-social-markdown": "\u{f4e6}",
    "ion-social-nodejs": "\u{f4e7}",
    "ion-social-octocat": "\u{f4e8}",
    "ion-social-pinterest": "\u{f2b1}",
    "ion-social-pinterest-outline": "\u{f2b0}",
    "ion-social-python": "\u{f4e9}",
    "ion-social-reddit": "\u{f23b}",
    "ion-social-reddit-outline": "\u{f23a}",
    "ion-social-rss": "\u{f23d}",
    "ion-social-rss-outline": "\u{f23c}",
    "ion-social-sass": "\u{f4ea}",
    "ion-social-skype": "\u{f23f}",
    "ion-social-skype-outline": "\u{f23e}",
    "ion-social-snapchat": "\u{f4ec}",
    "ion-social-snapchat-outline": "\u{f4eb}",
    "ion-social-tumblr": "\u{f241}",
    "ion-social-tumblr-outline": "\u{f240}",
    "ion-social-tux": "\u{f2c5}",
    "ion-social-twitch": "\u{f4ee}",
    "ion-social-twitch-outline": "\u{f4ed}",
    "ion-social-twitter": "\u{f243}",
    "ion-social-twitter-outline": "\u{f242}",
    "ion-social-usd": "\u{f353}",
    "ion-social-usd-outline": "\u{f352}",
    "ion-social-vimeo": "\u{f245}",
    "ion-social-vimeo-outline": "\u{f244}",
    "ion-social-whatsapp": "\u{f4f0}",
    "ion-social-whatsapp-outline": "\u{f4ef}",
    "ion-social-windows": "\u{f247}",
    "ion-social-windows-outline": "\u{f246}",
    "ion-social-wordpress": "\u{f249}",
    "ion-social-wordpress-outline": "\u{f248}",
    "ion-social-yahoo": "\u{f24b}",
    "ion-social-yahoo-outline": "\u{f24a}",
    "ion-social-yen": "\u{f4f2}",
    "ion-social-yen-outline": "\u{f4f1}",
    "ion-social-youtube": "\u{f24d}",
    "ion-social-youtube-outline": "\u{f24c}",
    "ion-soup-can": "\u{f4f4}",
    "ion-soup-can-outline": "\u{f4f3}",
    "ion-speakerphone": "\u{f2b2}",
    "ion-speedometer": "\u{f2b3}",
    "ion-spoon": "\u{f2b4}",
    "ion-star": "\u{f24e}",
    "ion-stats-bars": "\u{f2b5}",
    "ion-steam": "\u{f30b}",
    "ion-stop": "\u{f24f}",
    "ion-thermometer": "\u{f2b6}",
    "ion-thumbsdown": "\u{f250}",
    "ion-thumbsup": "\u{f251}",
    "ion-toggle": "\u{f355}",
    "ion-toggle-filled": "\u{f354}",
    "ion-transgender": "\u{f4f5}",
    "ion-trash-a": "\u{f252}",
    "ion-trash-b": "\u{f253}",
    "ion-trophy": "\u{f356}",
    "ion-tshirt": "\u{f4f7}",
    "ion-tshirt-outline": "\u{f4f6}",
    "ion-umbrella": "\u{f2b7}",
    "ion-university": "\u{f357}",
    "ion-unlocked": "\u{f254}",
    "ion-upload": "\u{f255}",
    "ion-usb": "\u{f2b8}",
    "ion-videocamera": "\u{f256}",
    "ion-volume-high": "\u{f257}",
    "ion-volume-low": "\u{f258}",
    "ion-volume-medium": "\u{f259}",
    "ion-volume-mute": "\u{f25a}",
    "ion-wand": "\u{f358}",
    "ion-waterdrop": "\u{f25b}",
    "ion-wifi": "\u{f25c}",
    "ion-wineglass": "\u{f2b9}",
    "ion-woman": "\u{f25d}",
    "ion-wrench": "\u{f2ba}",
    "ion-xbox": "\u{f30c}"
]
