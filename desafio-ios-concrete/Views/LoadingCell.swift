//
//  LoadingCell.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 03/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell: UITableViewCell {
    
    let indicator: UIActivityIndicatorView
    let padding: CGFloat = 10
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        indicator = UIActivityIndicatorView()
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupActivityIndicator()
        setupConstraints()
        indicator.startAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    private func setupActivityIndicator() {
        indicator.activityIndicatorViewStyle = .gray
        indicator.hidesWhenStopped = true
    }
    
    private func setupConstraints() {
        contentView.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.topAnchor.constraint(equalTo: contentView.topAnchor, constant: padding).isActive = true
        indicator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -padding).isActive = true
        indicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
    }
}
