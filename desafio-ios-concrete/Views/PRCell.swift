//
//  PRCell.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 04/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import UIKit

class PRCell: UITableViewCell {
    
    var titleLabel: UILabel
    var bodyLabel: UILabel
    var usernameLabel: UILabel
    var userImage: UIImageView
    private let hSpacing: CGFloat = 6
    private let vSpacing: CGFloat = 4
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        titleLabel = UILabel()
        bodyLabel = UILabel()
        usernameLabel = UILabel()
        userImage = UIImageView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        styleCell()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func styleCell() {
        titleLabel.textColor = DefaultColors.blue
        usernameLabel.textColor = DefaultColors.blue
        usernameLabel.font = UIFont.systemFont(ofSize: 12)
        bodyLabel.textColor = .gray
        bodyLabel.font = UIFont.systemFont(ofSize: 12)
        bodyLabel.numberOfLines = 2
    }
    
    private func setupConstraints() {
        addSubviews()
        
        setupTitleLabelConstraints()
        setupBodyLabelConstraints()
        setupUsernameLabelConstraints()
        setupUserImageConstraints()
    }
    
    private func addSubviews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(bodyLabel)
        contentView.addSubview(usernameLabel)
        contentView.addSubview(userImage)
    }
    
    private func setupTitleLabelConstraints() {
        let margins = contentView.layoutMarginsGuide
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
    }
    
    private func setupBodyLabelConstraints() {
        let margins = contentView.layoutMarginsGuide
        bodyLabel.translatesAutoresizingMaskIntoConstraints = false
        bodyLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: vSpacing).isActive = true
        bodyLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        bodyLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
    }
    
    private func setupUserImageConstraints() {
        let margins = contentView.layoutMarginsGuide
        userImage.translatesAutoresizingMaskIntoConstraints = false
        userImage.topAnchor.constraint(equalTo: bodyLabel.bottomAnchor, constant: vSpacing).isActive = true
        userImage.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        userImage.widthAnchor.constraint(equalToConstant: 30).isActive = true
        userImage.heightAnchor.constraint(equalToConstant: 30).isActive = true
        userImage.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
    }
    
    private func setupUsernameLabelConstraints() {
        let margins = contentView.layoutMarginsGuide
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameLabel.topAnchor.constraint(greaterThanOrEqualTo: userImage.topAnchor).isActive = true
        usernameLabel.leadingAnchor.constraint(equalTo: userImage.trailingAnchor, constant: hSpacing).isActive = true
        usernameLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        usernameLabel.bottomAnchor.constraint(lessThanOrEqualTo: margins.bottomAnchor).isActive = true
        usernameLabel.centerYAnchor.constraint(lessThanOrEqualTo: userImage.centerYAnchor).isActive = true
    }
    
}
