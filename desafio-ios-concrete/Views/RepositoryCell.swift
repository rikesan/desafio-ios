//
//  RepositoryCell.swift
//  desafio-ios-concrete
//
//  Created by Henrique Santiago on 02/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import Foundation
import UIKit

class RepositoryCell: UITableViewCell {
    
    var nameLabel: UILabel
    var descriptionLabel: UILabel
    var userImage: UIImageView
    var usernameLabel: UILabel
    var forkLabel: UILabel
    var starsLabel: UILabel
    private let userProfileContainer: UIView
    private let forkIcon: UILabel
    private let starsIcon: UILabel
    private let hSpacing: CGFloat = 6
    private let vSpacing: CGFloat = 4
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        nameLabel = UILabel()
        descriptionLabel = UILabel()
        userImage = UIImageView()
        usernameLabel = UILabel()
        forkLabel = UILabel()
        starsLabel = UILabel()
        forkIcon = UILabel()
        starsIcon = UILabel()
        userProfileContainer = UIView()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        styleCell()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func styleCell() {
        nameLabel.textColor = DefaultColors.blue
        nameLabel.font = UIFont.systemFont(ofSize: 20)
        usernameLabel.textColor = DefaultColors.blue
        forkIcon.attributedText = Icon.getIcon(.ion_fork_repo, fontSize: 20)
        starsIcon.attributedText = Icon.getIcon(.ion_ios_star, fontSize: 20)
        forkIcon.textColor = DefaultColors.beige
        starsIcon.textColor = DefaultColors.beige
        forkLabel.textColor = DefaultColors.beige
        starsLabel.textColor = DefaultColors.beige
        descriptionLabel.textColor = .gray
        descriptionLabel.font = UIFont.systemFont(ofSize: 12)
        usernameLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.numberOfLines = 2
    }
    
    func setupConstraints() {
        addSubviews()
        setupNameLabelConstraints()
        setupDescriptionLabelConstraints()
        setupForkIconConstraints()
        setupForkLabelConstraints()
        setupStarsIconConstraints()
        setupStarsLabelConstraints()
        setupUserProfileConstraints()
        setupUserImageConstraints()
        setupUsernameConstraints()
    }
    
    func addSubviews() {
        contentView.addSubview(nameLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(userProfileContainer)
        userProfileContainer.addSubview(userImage)
        userProfileContainer.addSubview(usernameLabel)
        contentView.addSubview(forkIcon)
        contentView.addSubview(forkLabel)
        contentView.addSubview(starsIcon)
        contentView.addSubview(starsLabel)
    }
    
    func setupNameLabelConstraints() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        nameLabel.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: userProfileContainer.leadingAnchor, constant: -hSpacing).isActive = true
    }
    
    func setupDescriptionLabelConstraints() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        descriptionLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: vSpacing).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: userProfileContainer.leadingAnchor, constant: -hSpacing).isActive = true
    }
    
    func setupForkIconConstraints() {
        forkIcon.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        forkIcon.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: vSpacing).isActive = true
        forkIcon.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
        forkIcon.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
    }
    
    func setupForkLabelConstraints() {
        forkLabel.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        forkLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: vSpacing).isActive = true
        forkLabel.leadingAnchor.constraint(equalTo: forkIcon.trailingAnchor, constant: hSpacing).isActive = true
        forkLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
    }
    
    func setupStarsIconConstraints() {
        starsIcon.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        starsIcon.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: vSpacing).isActive = true
        starsIcon.leadingAnchor.constraint(equalTo: forkLabel.trailingAnchor, constant: hSpacing).isActive = true
        starsIcon.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
    }
    
    func setupStarsLabelConstraints() {
        starsLabel.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
        starsLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: vSpacing).isActive = true
        starsLabel.leadingAnchor.constraint(equalTo: starsIcon.trailingAnchor, constant: hSpacing).isActive = true
        starsLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        starsLabel.trailingAnchor.constraint(lessThanOrEqualTo: userProfileContainer.leadingAnchor, constant: hSpacing).isActive = true
    }
    
    func setupUserProfileConstraints() {
        userProfileContainer.translatesAutoresizingMaskIntoConstraints = false
        let margins = contentView.layoutMarginsGuide
    
        userProfileContainer.topAnchor.constraint(greaterThanOrEqualTo: margins.topAnchor).isActive = true
        userProfileContainer.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
        userProfileContainer.bottomAnchor.constraint(lessThanOrEqualTo: margins.bottomAnchor).isActive = true
        userProfileContainer.centerYAnchor.constraint(equalTo: margins.centerYAnchor).isActive = true
        userProfileContainer.widthAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupUserImageConstraints() {
        userImage.translatesAutoresizingMaskIntoConstraints = false
    
        userImage.topAnchor.constraint(greaterThanOrEqualTo: userProfileContainer.topAnchor).isActive = true
        userImage.trailingAnchor.constraint(lessThanOrEqualTo: userProfileContainer.trailingAnchor).isActive = true
        userImage.leadingAnchor.constraint(greaterThanOrEqualTo: userProfileContainer.leadingAnchor).isActive = true
        userImage.centerXAnchor.constraint(equalTo: userProfileContainer.centerXAnchor).isActive = true
        userImage.heightAnchor.constraint(equalTo: userProfileContainer.widthAnchor).isActive = true
        userImage.widthAnchor.constraint(equalTo: userProfileContainer.widthAnchor).isActive = true
    }
    
    func setupUsernameConstraints() {
        usernameLabel.translatesAutoresizingMaskIntoConstraints = false
        usernameLabel.topAnchor.constraint(equalTo: userImage.bottomAnchor, constant: vSpacing).isActive = true
        usernameLabel.leadingAnchor.constraint(greaterThanOrEqualTo: userProfileContainer.leadingAnchor).isActive = true
        usernameLabel.bottomAnchor.constraint(equalTo: userProfileContainer.bottomAnchor).isActive = true
        usernameLabel.trailingAnchor.constraint(lessThanOrEqualTo: userProfileContainer.trailingAnchor).isActive = true
        usernameLabel.centerXAnchor.constraint(equalTo: userProfileContainer.centerXAnchor).isActive = true
        usernameLabel.widthAnchor.constraint(lessThanOrEqualTo: userProfileContainer.widthAnchor).isActive = true
    }
}
