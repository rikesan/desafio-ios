//
//  desafio_ios_concreteNetworkTests.swift
//  desafio-ios-concreteNetworkTests
//
//  Created by Henrique Santiago on 05/11/17.
//  Copyright © 2017 Henrique Santiago. All rights reserved.
//

import XCTest
@testable import desafio_ios_concrete

class desafio_ios_concreteNetworkTests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: .default)
    }
    
    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }
    
    func testValidRepoRequest() {
        let url = URL(string: NetworkAPI.repoEndpoint)
        let promise = expectation(description: "Successful response. Code: 200")
    
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testConversionToUser() {
        let json = """
        {
            "login": "test",
            "id": 123456,
            "avatar_url": "https://avatars1.githubusercontent.com/u/6407041?v=4"
        }
        """
        guard let data = json.data(using: .utf8) else {
            XCTAssertTrue(false)
            return
        }
        
        do {
            let parsedData = try NetworkHelper.decode(data: data, into: User.self)
            XCTAssert((parsedData as Any) is User)
        } catch {
            XCTFail()
        }
        
    }
    
    func testConversionToRepository() {
        let json = """
        {
            "id": 123,
            "name": "repo name",
            "description": null,
            "stargazers_count": 1,
            "forks_count": 1,
            "owner": {
                "login": "test",
                "id": 123456,
                "avatar_url": "https://avatars1.githubusercontent.com/u/6407041?v=4"
            },
            "pulls_url": "https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}"
        }
        """
        
        guard let data = json.data(using: .utf8) else {
            XCTAssertTrue(false)
            return
        }
        
        do {
            let parsedData = try NetworkHelper.decode(data: data, into: Repository.self)
            XCTAssert((parsedData as Any) is Repository)
        } catch {
            XCTFail()
        }
        
    }
    
    func testConversionToPullRequest() {
        let json = """
        {
            "title": "PR Title",
            "body": null,
            "user": {
                "login": "test",
                "id": 123456,
                "avatar_url": "https://avatars1.githubusercontent.com/u/6407041?v=4"
            },
            "html_url": "https://github.com/elastic/elasticsearch/pull/27269"
        }
        """
        
        guard let data = json.data(using: .utf8) else {
            XCTAssertTrue(false)
            return
        }
        
        do {
            let parsedData = try NetworkHelper.decode(data: data, into: PullRequest.self)
            XCTAssert((parsedData as Any) is PullRequest)
        } catch {
            XCTFail()
        }
        
    }
    
    func testConversionToRepositoryPage() {
        let json = """
        {
            "total_count": 1,
            "items": [
                {
                    "id": 123,
                    "name": "repo name",
                    "description": null,
                    "stargazers_count": 1,
                    "forks_count": 1,
                    "owner": {
                        "login": "test",
                        "id": 123456,
                        "avatar_url": "https://avatars1.githubusercontent.com/u/6407041?v=4"
                    },
                    "pulls_url": "https://api.github.com/repos/ReactiveX/RxJava/pulls{/number}"
                }
            ]
        }
        """
        
        guard let data = json.data(using: .utf8) else {
            XCTAssertTrue(false)
            return
        }
        
        do {
            let parsedData = try NetworkHelper.decode(data: data, into: RepositoryPage.self)
            XCTAssert((parsedData as Any) is RepositoryPage)
        } catch {
            XCTFail()
        }
    }
}
